import java.util.*;
import java.math.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;


class LiveCell extends Cell{
    static boolean sex = SimStart.sex;
    
    static Comparator<LiveCell> comp = new Comparator<LiveCell>(){
	    public int compare(LiveCell first, LiveCell second){
		String fs = first.gen.toString();
		String ss = second.gen.toString();
		return fs.compareTo(ss);
	    }
	};
    // if spatial stuff matters then the tiebreaker here is a problem
    static Comparator<LiveCell> byEng = new Comparator<LiveCell>(){
	    public int compare(LiveCell first, LiveCell second){
		double fe = first.energy;
		double se = second.energy;
		if(fe != se)
		    return (int)Math.signum(SimStart.selectionDirection*(fe - se));
		if(SimStart.fitFunc > 0 && first.distSum != second.distSum)
		    return (int)Math.signum(SimStart.selectionDirection*(second.distSum - first.distSum));
		return Cell.comp.compare(first, second);
	    }
	};

    int matAge = SimStart.matAge;
    static int maxAge = 300000;
    
    Genome gen;
    // Could probably stand to get rid of some of these arrays...
    double[] internalMolContent = new double[608];
    double[] prevMolContent = new double[608];
    double[] unusedMolContent = new double[608];
    Map<Integer, Reaction> internalReactions;
    private Map<Character, Map<Integer, Double>> repressors = new HashMap<>();
    double[] baseExpSums = new double[608];
    double[] baseReaSums = new double[608];
    double[] reaSums = new double[608];
    double[] expSums = new double[608];
    double[] impSums = new double[608];
    double[] imports = new double[608];
    double[] exports = new double[608];
    double[] produces = new double[608];
    double[] processes = new double[608];
    int lived; // age. dont know why i didnt call it age...
    int generation;
    int genesUsed;
    int birthday = SimStart.iternum;
    private int productsUsed;
    double selEng;
    int[][] pLocs;
    int[] pIterBorn;
    int born;
    double energy;
    double distSum;
    double startEnergy;
    double energyUsed;
    double energyProd;
    double energySurp;
    double molSum = 0;
    double unusedSum = 0;
    AtomicInteger numBabby = new AtomicInteger();
    int children = 0;
    boolean killed = false;
    
    double ptRate;
    double rcRate;
    boolean mutableRates = false;
    boolean doa;
    int numC;
    double bestEnergy;
    int bestAge = 0;
    boolean noNewGenes = false;
    int numGene;
    boolean infinite = false;
    ArrayList<Double> fitDist = new ArrayList<>();
    ArrayList<Double> distDist = new ArrayList<>();
    ArrayList<Double> engDist = new ArrayList<>();
    ArrayList<Integer> infDist = new ArrayList<>();
    
    private LiveCell(int[] loc, Environment e, Genome[] gen, double ptRate, double rcRate){
       	super(loc, e);
	this.ptRate = ptRate;
	this.rcRate = rcRate;
	initialize(gen);
	if(SimStart.selectionDirection < 0){
	    bestEnergy = Double.POSITIVE_INFINITY;
	}
	else{
	    bestEnergy = Double.NEGATIVE_INFINITY;
	}
    }


    LiveCell(int[] loc, Environment e, Genome gen, double ptRate, double rcRate){
	this(loc, e, new Genome[]{gen}, ptRate, rcRate);
    }

    LiveCell(int[] loc, Environment e, Genome gen){
	this(loc, e, gen, -1, -1);
    }
    
    LiveCell(int[] loc, Environment e, LiveCell old){
	this(loc, e, new Genome[]{old.gen}, old.ptRate, old.rcRate);
	matAge = SimStart.matAge;//old.matAge+((old.infinite)? 1 : 0);
	pLocs[0] = old.getLoc();
	pIterBorn[0] = old.born;
	generation = old.generation + 1;
    }

    LiveCell(int[] loc, Environment e, LiveCell old1, LiveCell old2){
	this(loc, e, new Genome[]{old1.gen, old2.gen}, old1.ptRate, old1.rcRate);
	generation = Math.max(old1.generation, old2.generation);
	matAge = SimStart.matAge;//Math.max(old1.matAge, old2.matAge);// + ((old1.infinite || old2.infinite)? 7 : 0);
	pLocs[0] = old1.getLoc();
	pLocs[1] = old2.getLoc();
	pIterBorn[0] = old1.born;
	pIterBorn[1] = old2.born;
    }


    public double getEnergy(){
	return energy;
    }

    public boolean reinit(){
	return initialize(new Genome[]{this.gen});
    }
    
    private boolean initialize(Genome[] gen){
	Arrays.fill(internalMolContent, 0);
	repressors.clear();
	Arrays.fill(baseExpSums, 0);
	Arrays.fill(baseReaSums, 0);
	Arrays.fill(reaSums, 0);
	Arrays.fill(expSums, 0);
	Arrays.fill(impSums, 0);
	numBabby = new AtomicInteger();
	children = 0;
	killed = false;
	infinite = false;
	noNewGenes = false;
	fitDist.clear();
	distDist.clear();
	engDist.clear();
	infDist.clear();
	if(mutableRates){
	    ptRate = ptRate;
	}
	    
	if(gen.length == 1)
	    this.gen = new Genome(gen[0], rand, ptRate, rcRate);
	else{
	    this.gen.initialize(gen[0], gen[1], rand, ptRate, rcRate);
	    //this.gen = new Genome(gen[0], gen[1], rand, ptRate, rcRate);
	}
	doa = this.gen.dead();
	numC = this.gen.numC;
	lived = 0;
	if(doa)
	    lived = -1;
	internalReactions = Collections.unmodifiableMap(this.gen.makeReactions(this));
	numGene = internalReactions.size();
	makeExprS(internalReactions);
	pLocs = new int[2][2];
	pLocs[0][0] = -1;
	pLocs[0][1] = -1;
	pLocs[1][0] = -1;
	pLocs[1][1] = -1;
	pIterBorn = new int[2];
	pIterBorn[0] = -1;
	pIterBorn[1] = -1;
	born = SimStart.iternum;

	if(SimStart.fitFunc == 0 || SimStart.fitFunc == 2){
	    distSum = 0;
	    for(int i = 0; i < internalMolContent.length; i++){
		if(pe.target[1][i] >= 0){
		    if(internalMolContent[i] < pe.target[0][i])
			distSum += Math.pow((internalMolContent[i] - pe.target[0][i]), 2);
		    else if(internalMolContent[i] > pe.target[1][i])
			distSum += Math.pow((internalMolContent[i] - pe.target[1][i]), 2);
		}
	    }
	    distSum = Math.sqrt(distSum);
	    if(SimStart.fitFunc == 0)
		energy = distSum;
	    else{
		if(distSum > 0)
		    energy = energy/distSum;
		else
		    energy = Double.POSITIVE_INFINITY;
	    }
	    if(energy == Double.POSITIVE_INFINITY)
		infinite = true;
	}
	diffuseMolContent();
	categorizeCell();
	return true;
    }

    public void categorizeCell(){
	if(doa || lived > maxAge){
	    pe.deadQueue.offer(this);
	}
	else if(lived > matAge){
	    if(infinite){
		pe.infQueue.offer(this);
	    }
	    else{
		pe.viableQueue.offer(this);
	    }
	}
	else{
	    pe.immaQueue.offer(this);
	}
    }
    
    void spike(char type, int which, double amount){
	if(!repressors.containsKey(type))
	    repressors.put(type, new HashMap<Integer, Double>());
	if(!repressors.get(type).containsKey(which))
	    repressors.get(type).put(which, 0.0);
	repressors.get(type).put(which, repressors.get(type).get(which) + amount);
    }

    private Map<Character, Map<Integer, Double>> repressorCopy(){
	Map<Character, Map<Integer, Double>> result = new HashMap<>();
	for(char type : repressors.keySet()){
	    if(!result.containsKey(type))
		result.put(type, new HashMap<Integer, Double>());
	    for(int which : repressors.get(type).keySet()){
		result.get(type).put(which, repressors.get(type).get(which));
	    }
	}
	return result;
    }
    
    private void makeExprS(Map<Integer, Reaction> rs){
	for(Reaction r : rs.values()){
	    if(r.type == 'e'){
		int m = r.reactionNum;
		baseExpSums[m] += r.exprSum;
	    }
	    else if(r.type == 'r'){
		for(int i = 0; i < 2; i++){		    
		    int m = r.ins[i];
		    baseReaSums[m] += r.exprSum;
		}
		 
	    }
	}
	updateExprS();
    }

    private void updateExprS(){
	System.arraycopy(baseReaSums, 0, reaSums, 0, baseReaSums.length);
	System.arraycopy(baseExpSums, 0, expSums, 0, baseExpSums.length);
	for(Reaction r : internalReactions.values()){
	    if(repressors.containsKey(r.type) && repressors.get(r.type).containsKey(r.reactionNum)){
		if(r.type == 'e'){
		    int m = r.reactionNum;
		    expSums[m] = expSums[m] - repressors.get('e').get(r.reactionNum);
		}
		else if(r.type == 'r'){
		    for(int i = 0; i < 2; i++){
			int m = r.ins[i];
			reaSums[m] = reaSums[m] - repressors.get('r').get(r.reactionNum);
		    }
		}
	    }
	}
    }
    
    public String sString(){
	String result = loc[0]+","+loc[1]+"\t"+lived+"\t" + numBabby.get() + "\t"+generation+"\t"+genesUsed+"\t"+energy+"\t"+energyProd+"\t"+energyUsed+"\t"+energySurp+"\t";
	for(Chromosome c : gen.gen){
	    result += c.len+",";
	}
	// stripping the final comma
	result = result.substring(0, result.length()-1);
	return result;
    }
    
    public String dump(){
	String result = "";
	result += "Generation:\t" + generation + "\nAge:\t" + lived +
	    "\nOffspring:\t" + numBabby.get() + 
	    "\nEnergy:\t" + energy + "\nEnergy produced:\t" + energyProd +
	    "\nEnergy used:\t" + energyUsed + "\nEnergy surp:\t" + energySurp + 
	    "\nStarting Energy:\t" + startEnergy + 
	    "\nReactions:";
	for(Reaction r : internalReactions.values()){
	    result += "\n" + r;
	}
	result += "\nMol Content";
	for(int b = 0; b < 608; b++){
	    if(internalMolContent[b] > 0)
		result += "\n" + b + "\t" + allMols.get(b) + "\t" + internalMolContent[b];
	}
	result += "\n" + gen + "\n";
	return result;
    }

    public Map<String, Object> dumpMap(){
	TreeMap<String, Object> dumpMap = new TreeMap<>();
	dumpMap.put("Genome", this.gen.toString());
	dumpMap.put("Age", lived);
	// im not proud of this
	dumpMap.put("Reactions", Arrays.stream(internalReactions.values().toArray(new Reaction[internalReactions.size()])).map(i->i.toMap()).toArray());
	dumpMap.put("Generation", generation);		   
	dumpMap.put("OffSpring", numBabby.get());
	dumpMap.put("Energy", energy);
	dumpMap.put("EnergyP", energyProd);
	dumpMap.put("EnergyU", energyUsed);
	dumpMap.put("EnergyS", energySurp);
	dumpMap.put("StartingEng", startEnergy);
	dumpMap.put("Mols", internalMolContent.clone());
	dumpMap.put("UnusedMols", unusedMolContent.clone());
	// if large numbers of repressors become a thing, this could be cumbersome
	// however as of writing this comment, there is only ever one repressor per
	// individual, so doing a manual deep copy is not that big a deal.
	dumpMap.put("Regulation", repressorCopy());
	if(SimStart.fitFunc == 2)
	    dumpMap.put("TarDist", distSum);
	dumpMap.put("FitTraj", fitDist.toArray());
	dumpMap.put("DistTraj", distDist.toArray());
	dumpMap.put("EngTraj", engDist.toArray());
	dumpMap.put("InfTraj", infDist.toArray());
	dumpMap.put("Imports", imports.clone());
	dumpMap.put("Exports", exports.clone());
	dumpMap.put("Processes", processes.clone());
	dumpMap.put("Produces", produces.clone());
	return dumpMap;
    }

    public String toString(){
	return "L";
    }

    public String dumpString(){
	StringBuilder result = new StringBuilder("Type:\tLive\n");
	result.append(super.baseString());
	result.append(dump());
	return result.toString();
    }

    private boolean reproduce(){
	// if this neutral cell has insufficient neighbors to be selected as candidates
	// for replication, this location remains unchanged, and return
	killed = false;
	if(pe.viableCount < (LiveCell.sex? 2 : 1)){
	    pe.nextBoard[loc[0]][loc[1]] = this;
	    //categozieCell();
	    //diffuseMolContent();
	    return false;
	}
	ArrayList<LiveCell> candNeigh = new ArrayList<LiveCell>();
	int ind = 0;
	matAge = SimStart.matAge;
	if(!LiveCell.sex){
	    LiveCell p = getParent();
	    initialize(new Genome[]{p.gen, p.gen});
	    pLocs[0] = p.getLoc();
	    pIterBorn[0] = p.born;
	    generation = p.generation + 1;
	}
	else{
	    LiveCell p1 = getParent();
	    LiveCell p2 = getParent();
	    if(SimStart.verbose)
		System.err.println(loc[0] + ", " + loc[1] + "\t" + p1.loc[0] + ", " + p1.loc[1] + "\t" + p2.loc[0] + ", " + p2.loc[1] + "\t*******************************");
	    try{
		initialize(new Genome[]{p1.gen, p2.gen});
	    }
	    catch(Exception e){
		System.err.println("p1: " + p1.loc[0] + ", " + p1.loc[1] + " killed? " + p1.killed);
		System.err.println("p2: " + p2.loc[0] + ", " + p2.loc[1] + " killed? " + p2.killed);
		throw(e);
	    }
	    generation = Math.max(p1.generation, p2.generation) + 1;
	    pLocs[0] = p1.getLoc();
	    pLocs[1] = p2.getLoc();
	    pIterBorn[0] = p1.born;
	    pIterBorn[1] = p2.born;
	}

	return true;
    }

    private LiveCell getParent(){
	// sometimes because of floating point error the last key will be slightly greater than one, but it shouldn't be very significant
	double intSelect = SimStart.r.nextDouble() * pe.breedTree.lastKey();
	LiveCell result;
	try{
	    result = pe.breedTree.ceilingEntry(intSelect).getValue();
	}
	catch(NullPointerException npe){
	    System.err.println("*** "+intSelect);
	    System.err.println(pe.breedTree.firstKey());
	    System.err.println(pe.breedTree.lastKey());
	    throw(npe);
	}
	result.numBabby.getAndIncrement();
	return result;
    }
    
    public LiveCell call(){
	if(killed)
	    reproduce();
	if(doa){
	    pe.nextBoard[loc[0]][loc[1]] = this;
	    categorizeCell();
	    return this;
	}
	molSum = 0;
	unusedSum = 0;
	energyUsed = 0;
	String out = "";

	for(int mc = 0 ; mc < internalMolContent.length; mc++){
	    molSum += internalMolContent[mc];
	    double thisUnused = Math.max(internalMolContent[mc] - reaSums[mc] - expSums[mc], 0);
	    unusedMolContent[mc] = thisUnused;
	    unusedSum += thisUnused;
	}

	System.arraycopy(internalMolContent, 0, prevMolContent, 0, internalMolContent.length);
	
	if(SimStart.selectionDirection * energy > SimStart.selectionDirection * bestEnergy){
	    bestEnergy = energy;
	    bestAge = lived;
	}
	startEnergy = energy;
	String output = "";
	energyProd = 0;
	energySurp = 0;
	int oldGenesUsed = genesUsed;
	genesUsed = 0;
	double efficiency = (energy > 0)? 1 : 0.;
	energyUsed = energy;
	// want cells to be able to have a chance to establish themselves regardless of starting energy
	if(lived < matAge - 1){
	    efficiency = 1;
	}
	energy = 0;

	Arrays.fill(imports, 0);
	Arrays.fill(exports, 0);
	Arrays.fill(processes, 0);
	Arrays.fill(produces, 0);

	if(efficiency == 1 && internalReactions.size() > 0){
	    if(SimStart.useReps)
		updateExprS();
	    for(Reaction r : internalReactions.values()){
		double energyChange = r.react(repressors.containsKey(r.type)?
					      ((repressors.get(r.type).containsKey(r.reactionNum))?
					       repressors.get(r.type).get(r.reactionNum) : 0.0) : 0.0, this);
		energyProd += energyChange;
		if(energyChange < 0){
		    energyUsed -= energyChange;
		}
		if(energyChange > 0){
		    energySurp += energyChange;
		}
		if(r.mExpr > 0){
		    genesUsed++;
		}
	    }
	    energy += energyProd;
	    energy = Math.max(energy, 0);
	}
	energy -= unusedSum*0.1;
	
	if(energy < 1){
	    energy = 1;
	}

	engDist.add(energy);

	if(SimStart.fitFunc == 0 || SimStart.fitFunc == 2){
	    distSum = 0;
	    for(int i = 0; i < internalMolContent.length; i++){
		if(pe.target[1][i] >= 0){
		    if(internalMolContent[i] < pe.target[0][i])
			distSum += Math.pow((internalMolContent[i] - pe.target[0][i]), 2);
		    else if(internalMolContent[i] > pe.target[1][i])
			distSum += Math.pow((internalMolContent[i] - pe.target[1][i]), 2);
		}
	    }

	    distSum = Math.sqrt(distSum);
	    if(SimStart.fitFunc == 0){
		energy = Math.max(pe.distMax - unusedSum*0.1, 1);
	    }
	    if(distSum > 0){
		infinite=false;
		energy = energy/distSum;// * pe.distMax;//(distSum/pe.distMax);
	    }
	    else if(!infinite){
		if(lived < matAge)
		    matAge = lived;
		infinite=true;
	    }
	}
	infDist.add((infinite)? 1 : 0);
	fitDist.add(energy);
	distDist.add(distSum);

	//energy = 1;
	lived++;
	pe.nextBoard[loc[0]][loc[1]] = this;
	pe.engTab[loc[0]][loc[1]] = energy;

	diffuseMolContent();
	categorizeCell();
	return this;
    }
}
