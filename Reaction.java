import java.util.*;
import java.math.*;
import java.util.concurrent.*;
import java.io.Console;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;

class Reaction{
    static final double baseReq = -0.01;
    // refers to the location of this reaction in the lists already generated
    final int reactionNum;
    // expression levels of this reaction
    final ArrayList<Double> exprs = new ArrayList<Double>();
    double exprSum = 0;
    // 'i'mport, 'e'xport, 'r'eaction
    final char type;
    static final int numImp = 53;
    static final int numExp = 608;
    // energy (-) required to perform this reaction
    double energyReq;
    // energy (+) gained from performing this reaction
    double energyGain;
    // energy that a reaction yielded for this iteration (dependent on available resources)
    double eP = 0;
    // the amount of throughput that all copies of this reaction have
    double throughput = 0;
    // list of molecules going into this reaction
    int[] ins = new int[2];
    // list of molecules leaving this reaction
    int[] outs = new int[2];
    int specnum = -1;
    double mExpr = 0;
    ArrayList<ArrayList<Integer>> locs = new ArrayList<ArrayList<Integer>>(2);

    ArrayList<Double> activityDist = new ArrayList<>();

    boolean sat = false;
    
    // Sometimes floating point error can cause a number to be subtracted by another
    // number which is supposed to be equal, but is very slightly different. If this
    // happens, absurdly small concentrations can be recorded. This value is used as
    // a threshold to make sure this doesn't happen. If the result of said subtraction
    // is smaller than this number, we assume that the result should have been 0.
    final static double zeroThresh = 1E-4;

    Reaction(int rn, char t, double e, int wc, int l){
	reactionNum= rn;
	exprs.add(e);
	exprSum += e;
	energyReq = baseReq*exprs.get(0);
	type = t;
	locs.add(new ArrayList<Integer>());
	locs.add(new ArrayList<Integer>());
	locs.get(wc).add(l);
	setupReaction();
    }

    Reaction(byte[] reactString, int wc, int l){
	locs.add(new ArrayList<Integer>());
	locs.add(new ArrayList<Integer>());
	locs.get(wc).add(l);
	// Determine the expression level
	exprs.add((reactString[1]&0xFF)/255.*5);
	//System.err.println(reactString.get(4*2, 8*2));
	//System.err.println(reactString.get(4*2, 8*2).toLongArray()[0]);
	//System.err.println(exprs.get(0));
	exprSum += exprs.get(0);	
	energyReq = baseReq*exprs.get(0);

	// Determine the type of protein. 0 = import, 1 = export, 2 = 3 = reaction
	// In the paper, they use 4 bases for this since it fits neatly into a byte, but all that
	// actually matters is the rightmost base since everything above that just adds a multiple
	// of 4. Making it only count a single base probably won't change the general behavior of 
	// this model though. Not in any way I can think of at any rate.
	int typeNum = reactString[2]&3;
	
	// Determine the specificity of the reaction. There are 5,020,279 legal reactions and 
	// 16,777,216 possible sequences in the specificity domain of the gene according to the 
	// paper. I found 4,761,923 by allowing 2 cuts per strand and allowing the same reaction to 
	// occur in multiple ways (e.g. 1221 + 121 => 12221 + 11 in 3 different ways depending
	// on where the 2 from 121 is inserted into 1221, so I count all 3 as different reactions) 
	// plus the 608 needed to account for import/export of a single molecule
	int spec = ((reactString[3]&0xFF)<<16) | ((reactString[4]&0xFF)<<8) | (reactString[5]&0xFF);
	specnum = spec;

	// Should probably swap 608 and 4761923 in for variables since magic numbers are bad
	// they're the max number of molecules and reactions respectively, for what it's worth
	switch(typeNum){
	    // Import
	case 0:
	    reactionNum = spec%numImp;
	    type = 'i';
	    break;
	    //Export
	case 1:
	    reactionNum = spec%numExp;
	    type = 'e';
	    break;
	    // Going to allow this to fall through since 2 and 3 are both the same
	case 2:
	case 3:
	    reactionNum = spec%Cell.reactions.length;
	    type = 'r';
	    break;
	default: 
	    reactionNum = -1;
	    type = 'z';
	    System.err.println("Something went horribly wrong. How's that for a helpful error?!");
	    SimStart.shutDownEverything();
	    System.exit(1);		
	}
	setupReaction();
    }

    public static int spec(byte[] reactString){
	int typeNum = reactString[2]&3;
	int spec = ((reactString[3]&0xFF)<<16) | ((reactString[4]&0xFF)<<8) | (reactString[5]&0xFF);
	int result = -1;
	switch(typeNum){
	case 0:
	    result = spec%numImp;
	    break;
	case 1:
	    result = spec%numExp+numImp;
	    break;
	case 2:
	case 3:
	    result = spec%Cell.reactions.length+numImp+numExp;
	    break;
	default:
	    System.err.println("Invalid spec somehow?");
	    System.err.println(typeNum);
	    System.exit(1);
	}
	return result;
    }

    public void update(byte[] reactString, int wc, int l){
	locs.get(wc).add(l);
	// Update expression level
	double newExpr = (reactString[1]&0xFF)/255.*5;
	exprs.add(newExpr);
	exprSum += newExpr;
	throughput += newExpr;
    }
    
    private void setupReaction(){
	if(type != 'r'){
	    if(type == 'i'){
		energyGain = 0;
		//outs[0] = Cell.allMols.get(reactionNum);
		outs[0] = reactionNum;
	    }
	    else{
		energyGain = 0;
		//ins[0] = Cell.allMols.get(reactionNum);
		ins[0] = reactionNum;
	    }
	    throughput = exprs.get(0);
	}
	else{
	    // lengths of big/small input and output molecules.
	    // used to determine how much energy is gained from this reaction
	    int bIn = 0;
	    int sIn = Integer.MAX_VALUE;
	    int bOut = 0;
	    int sOut = Integer.MAX_VALUE;
	    int[] spec = Cell.reactions[reactionNum];
	    // we're assuming that each reaction is 2 in -> 2 out
	    for(int i = 0; i < 2; i++){
		int thisMol = spec[i];
		ins[i] = thisMol;
		int sval = Cell.allMolsL.get(thisMol);
		if(sval < sIn)
		    sIn = sval;
		if(sval > bIn)
		    bIn = sval;
	    }
	    for(int i = 2; i < 4; i++){
		int thisMol = spec[i];
		outs[i-2] = thisMol;
		int sval = Cell.allMolsL.get(thisMol);
		if(sval < sOut)
		    sOut = sval;
		if(sval > bOut)
		    bOut = sval;
	    }
	    // lets see how this works...
	    energyGain = (bOut + 0.5 * sOut - bIn - 0.5 * sIn);
	    throughput = exprs.get(0);
	}
    }

    public String toString(){
	String result = "Locs:\n";
	for(int i = 0; i < locs.size(); i++){
	    for(int l : locs.get(i)){
		result += i + "," + l + "\n";
	    }
	}
	int rn = reactionNum;
	result += "Spec:\t" + rn + "\n";
	switch(type){
	case 'r':	    
	    result  = "Reaction:\n" + result;
	    result += "Eng: " + eP + "\n";
	    int[] subsNProds = Cell.reactions[rn];
	    result += "Substrates:\n" + Long.toString(Cell.allMols.get(subsNProds[0]), 4) + "\n" +
		Long.toString(Cell.allMols.get(subsNProds[1]), 4) + "\n";
	    result += "Products:\n" + Long.toString(Cell.allMols.get(subsNProds[2]), 4) + "\n" +
		Long.toString(Cell.allMols.get(subsNProds[3]), 4) + "\n";
	    break;
	case 'i':
	    result = "Import:\n" + result;
	    result += Long.toString(Cell.allMols.get(rn), 4) + "\n";
	    break;
	case 'e':
	    result = "Export:\n" + result;
	    result += Long.toString(Cell.allMols.get(rn), 4) + "\n";
	    break;
	case 'b':
	    result = "Broken:\n";
	    break;
	default:
	    result += "Default?" + type + "\n";
	    break;
	}
	result += "Throughput: " + throughput;
	return result;
    }

    public Map<String, Object> toMap(){
	//ObjectMapper m = new ObjectMapper();
	HashMap<String, Object> rMap = new HashMap<String, Object>();
	rMap.put("Locs", locs);
	int rn = reactionNum;
	rMap.put("Spec", rn);
	rMap.put("Type", type);
	switch(type){
	case 'r':
	    rMap.put("Reaction", Cell.reactions[rn]);
	    rMap.put("EngP", eP);
	    break;
	case 'i':
	    rMap.put("Import", Cell.allMols.get(rn));
	    break;
	case 'e':
	    rMap.put("Export", Cell.allMols.get(rn));
	    break;
	case 'b':
	    rMap.put("Broken!", "HOW COULD YOU??");
	    break;
	default:
	    rMap.put("Default?", type);
	    break;
	}
	rMap.put("Throughput", throughput);
	rMap.put("Exprs", exprs.toArray());
	rMap.put("Activity", activityDist.toArray());
	/*
	String rString = "";
	try{
	    rString = m.writeValueAsString(rMap);
	}
	catch(JsonProcessingException e){
	    System.err.println(e.getMessage());
	    System.exit(1);
	}
	return rString;
	*/
	return rMap;
    }
    
    public double react(double repress, LiveCell host){
	switch(type){
	case 'i':
	    return importReact(repress, host);
	case 'e':
	    return exportReact(repress, host);
	case 'r':
	    return internalReact(repress, host);
	case 'b':
	    return 0;
	default:
	    // Should probably make this an exception at some point
	    System.err.println("Invalid reaction type!");
	    System.exit(1);
	}
	// this option should really never occur... I should probably make it crash if this happens.
	return 0;
    }

    private double importReact(double repress, LiveCell host){
	int target = outs[0];
	double change = 0;
	change = Math.min(throughput - repress, host.pe.molContent[target] + host.modContent[target]);
	change = Math.min(change, 2 * host.reaSums[target] - host.prevMolContent[target]);
	change = Math.max(change, 0);
	mExpr = change;
	host.internalMolContent[target] += change;
	double mCto = host.modContent[target] - change;
	activityDist.add(change);
	host.imports[target] += change;
	host.modContent[target] = mCto;
	return energyGain;
    }
	
    private double exportReact(double repress, LiveCell host){
	int target = ins[0];
	mExpr = Math.max(Math.min((host.prevMolContent[target]-host.reaSums[target])/throughput,1), 0);
	double change = 0;
	change = Math.max(Math.min(throughput-repress, mExpr * throughput), 0.0);
	if(Double.isInfinite(change) || Double.isNaN(change)){
	    System.err.println("export is a problem");
	    System.err.println(change);
	    System.err.println(host.prevMolContent[target]);
	    System.err.println(host.expSums[target]);
	    System.err.println(host.reaSums[target]);
	    System.err.println(throughput);
	    System.exit(1);
	}
	double iCto = host.internalMolContent[target] - change;
	if(iCto < zeroThresh){
	    iCto = 0;
	    if(iCto < 0)
		System.out.println("e " + iCto);
	}
	host.internalMolContent[target] = iCto;
	host.modContent[target] = host.modContent[target] + change;
	activityDist.add(change);
	host.exports[target] += change;
	return energyGain;
    }
	
    private double internalReact(double repress, LiveCell host){
	int target1 = ins[0];
	int target2 = ins[1];
	int target3 = outs[0];
	int target4 = outs[1];
	double change = 0;
	double test1 = host.prevMolContent[target1] / host.reaSums[target1];
	double test2 = host.prevMolContent[target2] / host.reaSums[target2];
	mExpr = Math.min(Math.min(test1, test2), 1);
	change = Math.max(Math.min(throughput - repress, mExpr*throughput), 0.0);
	double iCto = host.internalMolContent[target1] - change;
	if(iCto < zeroThresh){
	    iCto = 0;
	}
	host.internalMolContent[target1] = iCto;

	iCto = host.internalMolContent[target2] - change;
	if(iCto < zeroThresh){
	    iCto = 0;
	}
	host.internalMolContent[target2] = iCto;
		
	host.internalMolContent[target3] += change;
	host.internalMolContent[target4] += change;

	eP = energyGain * change;

	activityDist.add(change);
	host.processes[target1] += change;
	host.processes[target2] += change;
	host.produces[target3] += change;
	host.produces[target4] += change;
	    
	
	return energyGain * change;
    }

    public static boolean checkReaction(ArrayList<String> spec){
	String one = spec.get(0);
	String two = spec.get(1);
	String three = spec.get(2);
	String four = spec.get(3);
	int one3 = findMissMatch(one, three);
	int one4 = findMissMatch(one, four);
	int two3 = findMissMatch(two, three);
	int two4 = findMissMatch(two, four);
	return false;
    }

    // returns the first index at which the two input strings differ. If they're identical,
    // return -1
    public static int findMissMatch(String one, String two){
	for(int i = 0; i < Math.min(one.length(), two.length()); i++){
	    if(!one.regionMatches(0, two, 0, i))
		return i;
	}
	return -1;
    }

    private static String readTarget(String message, Console c){
	String result;
	while(true){
	    result = c.readLine(message);
	    try{
		Long.parseLong(result, 4);
	    }
	    catch(NumberFormatException e){
		System.out.println(result + " is not a valid base 4 integer!");
		continue;
	    }
	    if(!testMolSeq(result)){
		System.out.println(result + " is not a valid molecule!");
		continue;
	    }
	    if(result.length() > 24){
		System.out.println(result + " is " + result.length() + " bases long. The max allowed is 24.");
		continue;
	    }
	    break;
	}
	return result;
    }

    public static boolean testMolSeq(String m){
	int has;
	int needs = 0;
	for(int i = 0; i < m.length(); i++){
	    has = Integer.parseInt(m.charAt(i) + "");
	    needs = has - needs;
	    if(needs < 0)
		return false;
	}
	return needs == 0;
    }
}
