// Simple helper class to help with molecule diffusal. This is NOT
// designed to be used for anything aside from passing a matched integer
// (mol number) and double (concentration), so please don't use this for other
// things.

class Pair{
    final int left;
    final double right;
    public Pair(int left, double right){
	this.left = left;
	this.right = right;
    }
}
