import java.util.*;
import java.math.*;
import java.io.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.*;

// Gotta use generics here to facilitate the builder pattern and not have the compiler give warnings 
// about unchecked type casting in the constructor for Cell
//
// may want to re-think the construction pattern to use. The motivation behind implementing a builder
// pattern was to allow more flexible cell creation, but I'm not sure it's the right tool for the 
// job to be honest
abstract class Cell implements Callable<LiveCell>{
    static final double flowDensity = 0.5;
    static final List<Long> allMols = Collections.unmodifiableList(genLegalMols());
    static final List<Integer> allMolsL = Collections.unmodifiableList(makeLens(allMols));
    // turn em into base 4 longs so we can do easy bitwise operations
    private static List<Long> genLegalMols(){
	List<Long> temp = PreProcSpec.genLegalMols();
	List<Long> result = new ArrayList<Long>();
	for(long m : temp){
	    result.add(Long.parseLong(Long.toString(m), 4));
	}
	return result;
    }
    private static List<Integer> makeLens(List<Long> inL){
	List<Integer> result = new ArrayList<Integer>();
	for(long l : inL){
	    result.add(Long.toString(l,4).length());
	}
	return result;
    }
    static final int[][] reactions = initializeReactions();
    private static int[][] initializeReactions(){
	System.out.println("Reading reactions from file...");
	int[][] result = new int[1][1];
	try{
	    int[][] fromFile = PreProcSpec.reactionsFromFile("reactions.txt");
	    // this is a hack. I am a hack.
	    int numReact = fromFile.length;
	    if(SimStart.setReactNum > 0)
		numReact = SimStart.setReactNum;
	    result = new int[numReact][4];
	    for(int i = 0; i < numReact; i++){
		for(int j = 0; j < fromFile[i].length; j++){
		    result[i][j] = fromFile[i][j];
		}
	    }
	}
	catch(IOException e){
	    System.err.println("Something is wrong with reactions.txt");
	    System.exit(1);
	}
	System.out.println("Done.");
	return result;
    }

    static Comparator<Cell> comp = new Comparator<Cell>(){
	    public int compare(Cell first, Cell second){
		int[] floc = first.loc;
		int[] sloc = second.loc;
		//System.out.println(floc[0] + ", " + floc[1] + "\t" + sloc[0] + ", " + sloc[1]);
		return (((Integer)floc[0]).compareTo(sloc[0]) != 0)? ((Integer)floc[0]).compareTo(sloc[0]) : ((Integer)floc[1]).compareTo(sloc[1]);
	    }
	};

    int[] loc;
	
    double[] modContent = new double[608];

    Environment pe;
    Random rand;

    Cell(Cell other){
	loc = other.loc;
	pe = other.pe;
    }

    Cell(Cell old, Environment e){
	loc = old.loc;
	pe = e;
	rand = pe.boardRands[loc[0]][loc[1]];
	if(pe.needsNewRand.contains(loc)){
	    Thread.currentThread().dumpStack();
	}
	pe.needsNewRand.add(loc);
    }

    protected Cell(){
	loc = new int[2];
    }
	
    Cell(int[] loc, Environment e){
	this.loc = loc;
	pe = e;
	rand = pe.boardRands[loc[0]][loc[1]];
	pe.needsNewRand.add(loc);
    }
    
    public abstract String toString();

    public abstract String dumpString();

    public abstract Map<String, Object> dumpMap();
    
    public abstract LiveCell call();

    public String baseString(){
	StringBuilder result = new StringBuilder("Location:\t" + loc[0] + ", " + loc[1] + "\n");
	return result.toString();
    }

    // BECAUSE MAYBE WE'LL USE MORE THAN 2 DIMENSIONAL ENVIRONMENTS OK??
    public int[] getLoc(){
	int[] result = new int[loc.length];
	for(int i = 0; i < loc.length; i++){
	    result[i] = loc[i];
	}
	return result;
    }


    // puts the values by which the environmental molecular content is to be
    // modified into a queue which will then be used to update those values
    // in a way that will hopefully solve some inconsistency issues

    public void diffuseMolContent(){
	for(int mol = 0; mol < 608; mol++){
	    if(modContent[mol] != 0){
		if(!pe.diffuseQueue.offer(new Pair(mol, modContent[mol]))){
		    System.err.println("Somehow queue is full?");
		    SimStart.shutDownEverything();
		    System.exit(1);
		}
		else
		    modContent[mol] = 0.0;
	    }
	}
    }

}

