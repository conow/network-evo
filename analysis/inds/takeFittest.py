import json
import gzip
import argparse as ap
import os
from multiprocessing import Pool

p = ap.ArgumentParser()
p.add_argument('f')
args = p.parse_args()


def processFile(fn):
    with gzip.open(fn, 'rb') as gf:
        pop = json.load(gf)
        
    height = pop['Height']
    width = pop['Width']

    fi = 0
    fj = 0
    fc = 0
    fv = 0

    for i in range(height):
        for j in range(width):
            ind = pop['[{}, {}]'.format(i, j)]
            if ind['OffSpring'] > fv:
                fi = i
                fj = j
                fc = ind
                fv = ind['OffSpring']

    with gzip.open('{}_fittest.out.gz'.format(fn.rstrip('out.gz')), 'wt') as t:
        nj = dict()
        nj['Chosen'] = '[{}, {}]'.format(fi, fj)
        nj['Height'] = 1
        nj['Width'] = 1
        nj['[0, 0]'] = pop['[{}, {}]'.format(fi, fj)]
        nj['Concentrations'] = pop['Concentrations']
        nj['Provides'] = pop['Provides']
        json.dump(nj, t)
        print(fn)


ratesBase = args.f

envTypes = ('rich', 'limited', 'fast', 'slow')

flist = list()

for e in envTypes:
    for rate in os.listdir('{}/{}/'.format(ratesBase, e)):
        for r in os.listdir('{}/{}/{}/s_r/'.format(ratesBase, e, rate)):
            flist.append('{}/{}/{}/s_r/{}/post/250000.out.gz'.format(ratesBase, e, rate, r))
            flist.append('{}/{}/{}/s_r/{}/post/0.out.gz'.format(ratesBase, e, rate, r))

if __name__ == '__main__':
    p = Pool()
    running = list()
    while len(flist) > 0:        
        p.apply_async(processFile, (flist.pop(),))
    p.close()
    p.join()
