import std.stdio;
import std.math;
import std.conv;
import std.container;
import std.algorithm;
import std.file;
import std.path;
import std.range;
import std.string;
import std.typecons;
import std.exception;
import std.datetime;
import std.getopt;
import std.zlib;
import std.json;
import std.functional;


immutable Tuple!(string[608], immutable int[string]) legalMols;
double[608] envProds;

static this(){
  string[608] result1;
  int[string] result2;
  result1[0] = "11";
  result2["11"] = 0;
  result1[1] = "22";
  result2["22"] = 1;
  result1[2] = "33";
  result2["33"] = 2;
  string[] previous = result1[0..3];
  int back = 3;
  for(int i = 2; i < 12; i++){
    int c = 0;
    foreach(string p ; previous){
      switch(p[$-1]){
      case '1':
	result1[back+c] = p[0..$-1]~"32";
	result2[p[0..$-1]~"32"] = back+c;
	result1[back+c+1] = p[0..$-1]~"21";
	result2[p[0..$-1]~"21"] = back+c+1;
	c+=2;
	break;
      case '2':
	result1[back+c] = p[0..$-1]~"31";
	result2[p[0..$-1]~"31"] = back+c;
	c+=1;
	break;
      case '3':
	break;
      default:
	assert(0);
      }
    }
    previous = result1[back..back+c];
    back = back + c;
  }
  result2.rehash;
  auto result3 = assumeUnique(result2);
  legalMols = Tuple!(string[608], immutable int[string])(result1, result3);

}

void main(string[] args){
  auto start = Clock.currTime();
  string[] fnames;
  string[] gnames;
  for(int i = 1; i < args.length; i++){
    if(args[i].isDir){
      foreach(string fname ; dirEntries(args[i], SpanMode.shallow)){
	if(extension(fname) == ".gz")
	  gnames~=fname;
      }
    }
    else{
      if(extension(args[i]) == ".gz")
	gnames~=args[i];
    }
  }

  foreach(gname ; gnames){
    makeStatsG(gname);
  }
  writeln(Clock.currTime() - start);
}


bool makeStatsG(string gzipf){  
  Cell[] cellPop;
  writeln(gzipf);
  auto dumpFile = File(gzipf, "r");
  auto uc = new UnCompress();
  string dumpOut = "";
  auto timerStart = Clock.currTime();
  foreach(ubyte[] chunk ; chunks(dumpFile, 256000)){
    dumpOut ~= cast(string)uc.uncompress(chunk.dup);
  }
  writeln("Decompression time: ", Clock.currTime() - timerStart);
  timerStart = Clock.currTime();
  dumpOut ~= cast(string)uc.flush();
  auto dumpJ = parseJSON(dumpOut);
  writeln("Parse time: ", Clock.currTime() - timerStart);
  bool live = false;
  auto ccount = 0;
  auto oldFiles = [dumpFile.name[0..$-3]~".netstats",
		   dumpFile.name[0..$-3]~".iterstats",
		   dumpFile.name[0..$-3]~".flow",
		   dumpFile.name[0..$-3]~".thru",
		   dumpFile.name[0..$-3]~".prop",
		   dumpFile.name[0..$-3]~".cconT",
		   dumpFile.name[0..$-3]~".cellT",
		   dumpFile.name[0..$-3]~".nconT",
		   dumpFile.name[0..$-3]~".netT"];
  foreach(oldFile ; oldFiles){
    if(exists(oldFile))
      remove(oldFile);
  }
		   
  auto newOut = File(dumpFile.name[0..$-3]~".netstats.gz", "w");
  auto statsOut = File(dumpFile.name[0..$-3]~".iterstats.gz", "w");
  auto flowOut = File(dumpFile.name[0..$-3]~".flow.gz", "w");
  auto thruOut = File(dumpFile.name[0..$-3]~".thru.gz", "w");
  auto propOut = File(dumpFile.name[0..$-3]~".prop.gz", "w");
  auto bigProp = File(dumpFile.name[0..$-3]~".prop.big.gz", "w");
  auto bigThru = File(dumpFile.name[0..$-3]~".thru.big.gz", "w");
  auto bigFlow = File(dumpFile.name[0..$-3]~".flow.big.gz", "w");
  auto avgProp = File(dumpFile.name[0..$-3]~".prop.avg.gz", "w");
  auto avgThru = File(dumpFile.name[0..$-3]~".thru.avg.gz", "w");
  auto avgFlow = File(dumpFile.name[0..$-3]~".flow.avg.gz", "w");
  auto stdevProp = File(dumpFile.name[0..$-3]~".prop.stdev.gz", "w");
  auto stdevThru = File(dumpFile.name[0..$-3]~".thru.stdev.gz", "w");
  auto stdevFlow = File(dumpFile.name[0..$-3]~".flow.stdev.gz", "w");
  auto pathsOut = File(dumpFile.name[0..$-3]~".paths.gz", "w");
  auto newComp = new Compress(HeaderFormat.gzip);
  auto statsComp = new Compress(HeaderFormat.gzip);
  auto flowComp = new Compress(HeaderFormat.gzip);
  auto thruComp = new Compress(HeaderFormat.gzip);
  auto propComp = new Compress(HeaderFormat.gzip);
  auto bigPComp = new Compress(HeaderFormat.gzip);
  auto bigTComp = new Compress(HeaderFormat.gzip);
  auto bigFComp = new Compress(HeaderFormat.gzip);
  auto avgPComp = new Compress(HeaderFormat.gzip);
  auto avgTComp = new Compress(HeaderFormat.gzip);
  auto avgFComp = new Compress(HeaderFormat.gzip);
  auto stdevPComp = new Compress(HeaderFormat.gzip);
  auto stdevTComp = new Compress(HeaderFormat.gzip);
  auto stdevFComp = new Compress(HeaderFormat.gzip);
  auto pathsComp = new Compress(HeaderFormat.gzip);
  //newOut.writeln

  auto longHeader = new RedBlackTree!string();
  double[][Gene][Gene] allFlow;
  double[][Gene][Gene] allThru;
  double[][Gene][Gene] allProp;
  RedBlackTree!Gene gSet = new RedBlackTree!Gene();
  RedBlackTree!Gene g2Set = new RedBlackTree!Gene();
  
  newOut.rawWrite(newComp.compress(join(["loc", "gen", "ccount",
					 "nid","type", "cop",
					 "nImp", "sImp",
					 "engP",
					 "spec", "rep", "pathn", "mapathi", "mipathi", "apathi", "vpathi",
					 "ideg", "odeg"], "\t") ~ "\n"));
  statsOut.rawWrite(statsComp.compress(join(["loc", "gen", "age", "eng", "engp", "engs", "engu","ugene", "tgene", "offnum", "ccount"],"\t")~"\n"));
  envProds = map!(a => a.floating)(dumpJ["Provides"].array).array;
  long width = dumpJ["Width"].integer;
  long height = dumpJ["Height"].integer;
  for(int w = 0; w < width; w++){
    for(int h = 0; h < height; h++){
      string locS = "[" ~ to!string(h) ~ ", " ~ to!string(w) ~ "]";
      //writeln(locS);
      if(locS !in dumpJ)
	continue;
      if("Type" in dumpJ[locS])
	continue;
      auto tc = dumpJ[locS];
      int[2] loc = [h, w];
      int gen = to!int(tc["Generation"].integer);
      int age = to!int(tc["Age"].integer);
      double eng = tc["Energy"].floating;
      double engp = tc["EnergyP"].floating;
      double engs = tc["EnergyS"].floating;
      double engu = tc["EnergyU"].floating;
      int offnum = to!int(tc["OffSpring"].integer);
      string[2] genome = tc["Genome"].str.stripRight.split("\n");
      int chromCount = (genome[0] == "@" || genome[1] == "@")? 1 : 2;
      Gene[string] genes;
      int gcount = 0;
      double thr;
      int count = 0;
      Tuple!(Gene, double[2])[] orderedExpr;
      auto reactList = tc["Reactions"].array;
      foreach(react ; reactList){
	auto rtype = to!char(react["Type"].str);
	int[2] ins;
	ins[] = -1;
	int[2] outs;
	outs = -1;
	long spec = react["Spec"].integer;
	switch(rtype){
	case 'r':
	  auto tempR = react["Reaction"].array;
	  ins[0] = to!int(tempR[0].integer);
	  ins[1] = to!int(tempR[1].integer);
	  outs[0] = to!int(tempR[2].integer);
	  outs[1] = to!int(tempR[3].integer);
	  thr = react["Throughput"].floating;
	  gcount = to!int((react["Locs"].array)[0].array.length) + to!int((react["Locs"].array)[1].array.length);
	  count += gcount;
	  break;
	case 'i':
	  outs[0] = to!int(react["Spec"].integer)%53;
	  thr = react["Throughput"].floating;
	  gcount = to!int((react["Locs"].array)[0].array.length) + to!int((react["Locs"].array)[1].array.length);
	  count += gcount;
	  break;
	case 'e':
	  ins[0] = to!int(react["Spec"].integer)%608;
	  thr = react["Throughput"].floating;
	  gcount = to!int((react["Locs"].array)[0].array.length) + to!int((react["Locs"].array)[1].array.length);
	  count += gcount;
	  break;
	default:
	  break;
	}
	auto exprs = map!(a => a.floating)(react["Exprs"].array).array;
	if(rtype != 'n' && thr > 0){
	  string repstr = to!string(ins) ~ "->" ~ to!string(outs);
	  if(repstr in genes){
	    writeln("Well that shouldn't happen");
	    assert(0);
	  }
	  genes[repstr] = new Gene(rtype, ins, outs, exprs, thr, gcount, spec);
	  orderedExpr~=Tuple!(Gene, double[2])(genes[repstr], [thr, 1.0]);
	}
      }
      auto regulate = tc["Regulation"];
      auto c = new Cell(loc, gen, age, eng, engp, genes, count, orderedExpr);
      statsOut.rawWrite(statsComp.compress(join([to!string(c.loc), to!string(gen), to!string(age),
						 to!string(eng), to!string(engp), to!string(engs), to!string(engu),
						 to!string(genes.length), to!string(count), to!string(offnum), to!string(chromCount)],"\t")~"\n"));
      foreach(Network n ; c.nets){
	string header;
	foreach(Gene g ; n.genes){
	  header ~= g.reactType~"\t";
	}
	foreach(Gene g ; n.genes){
	  foreach(path ; g.dfspaths){
	    pathsOut.rawWrite(pathsComp.compress(join([to!string(c.loc), to!string(g.spec), to!string(g.rep), to!string(path)], "\t")~"\n"));
	  }
	  newOut.rawWrite(newComp.compress(join([to!string(c.loc), to!string(c.gen), to!string(chromCount),
					      to!string(n.id), to!string(g.reactType), to!string(g.count),
					      to!string(g.knockoutRemImp), to!string(g.netSplitImp),
					      to!string(g.engB), to!string(g.spec), to!string(g.rep),
					      to!string(g.paths.length), to!string(g.pathma),
					      to!string(g.pathmi), to!string(g.patha), to!string(g.pathv),
					      to!string(g.iGs.length), to!string(g.oGs.length)],"\t")~"\n"));
	  // I know ranges and streams are all the rage to make things more concise and "readable"
	  // but it is easier for me to reason things out by breaking things into loops so here
	  double[] flowVec;
	  double[] thruVec;
	  double[] propVec;
	  foreach(Gene g2 ; n.genes){
	    if(g2 in g.flowFrom){
	      flowVec ~= g.flowFrom[g2];
	      thruVec ~= g.thruFrom[g2];
	      propVec ~= g.propFrom[g2];
	    }	    
	    else{
	      flowVec ~= 0.0;
	      thruVec ~= 0.0;
	      propVec ~= 0.0;
	    }
	  }	  
	  flowOut.rawWrite(flowComp.compress(join([to!string(c.loc), to!string(n.id), to!string(g.rep)]~to!(string[])(flowVec), "\t")~"\n"));
	  thruOut.rawWrite(thruComp.compress(join([to!string(c.loc), to!string(n.id), to!string(g.rep)]~to!(string[])(thruVec), "\t")~"\n"));
	  propOut.rawWrite(propComp.compress(join([to!string(c.loc), to!string(n.id), to!string(g.rep)]~to!(string[])(propVec), "\t")~"\n"));
	}
      }
      string header = to!string(c.loc) ~ "\t" ~ join(to!(string[])(map!(b => b.rep)(filter!(a => a.reactType != 'e')(c.allGenes[])).array), "\t");
      longHeader.insert(to!(string[])(map!(b => b.rep)(filter!(a => a.reactType != 'e')(c.allGenes[])).array));
      
      bigProp.rawWrite(bigPComp.compress(header~"\n"));
      bigThru.rawWrite(bigTComp.compress(header~"\n"));
      bigFlow.rawWrite(bigFComp.compress(header~"\n"));
      gSet.insert(filter!(a=>a.reactType != 'i')(c.allGenes[]));
      g2Set.insert(filter!(a=>a.reactType != 'e')(c.allGenes[]));
      foreach(g ; c.allGenes){
	if(g.reactType == 'i')
	  continue;
	double[] gvec;
	double[] tvec;
	double[] fvec;
	foreach(g2 ; c.allGenes){
	  if(g2.reactType == 'e')
	    continue;
	  if(g2 in g.propFrom){
	    gvec ~= g.propFrom[g2];
 	    allProp[g][g2] ~= [g.propFrom[g2]];	    
	    tvec ~= g.thruFrom[g2];
	    allThru[g][g2] ~= g.thruFrom[g2];
	    fvec ~= g.flowFrom[g2];
	    allFlow[g][g2] ~= g.flowFrom[g2];
	  }
	  else{
	    tvec ~= 0.0;
	    gvec ~= 0.0;
	    fvec ~= 0.0;
	    allProp[g][g2] ~= 0;
	    allThru[g][g2] ~= 0;
	    allFlow[g][g2] ~= 0;
	    
	  }
	}
	bigProp.rawWrite(bigPComp.compress(join([to!string(g.rep)]~to!(string[])(gvec), "\t")~"\n"));
	bigThru.rawWrite(bigTComp.compress(join([to!string(g.rep)]~to!(string[])(tvec), "\t")~"\n"));
	bigFlow.rawWrite(bigFComp.compress(join([to!string(g.rep)]~to!(string[])(fvec), "\t")~"\n"));
      }
      //return true;
    }
  }
  
  string header = "[all]" ~ "\t" ~ join(longHeader.array.sort(), "\t");
  avgProp.rawWrite(avgPComp.compress(header~"\n"));
  avgThru.rawWrite(avgTComp.compress(header~"\n"));
  avgFlow.rawWrite(avgFComp.compress(header~"\n"));
  stdevProp.rawWrite(stdevPComp.compress(header~"\n"));
  stdevThru.rawWrite(stdevTComp.compress(header~"\n"));
  stdevFlow.rawWrite(stdevFComp.compress(header~"\n"));

  long popsize = width * height;
  
  foreach(g ; gSet){
    double[] agvec;
    double[] atvec;
    double[] afvec;
    double[] sgvec;
    double[] stvec;
    double[] sfvec;
    
    foreach(g2 ; g2Set){
      /* I don't think I want to do this?
      if(allProp.length < popsize){
	int[] zeros;
	zeros.length = popsize - allProp.length;
	allProp[g][g2] ~= zeros.dup;
	allThru[g][g2] ~= zeros.dup;
	allFlow[g][g2] ~= zeros.dup;
      }
      */
      if(g2 in allProp[g]){
	agvec ~= sum(allProp[g][g2])/float(allProp[g][g2].length);
	atvec ~= sum(allThru[g][g2])/float(allThru[g][g2].length);
	afvec ~= sum(allFlow[g][g2])/float(allFlow[g][g2].length);
	double[] temp;
	temp.length = allProp[g][g2].length;
	temp[] = allProp[g][g2][] - agvec[$-1];
	sgvec ~= sqrt(sum(map!(a => a*a)(temp))/float(allProp[g][g2].length));
	temp[] = allThru[g][g2][] - atvec[$-1];
	stvec ~= sqrt(sum(map!(a => a*a)(temp))/float(allThru[g][g2].length));
	temp[] = allFlow[g][g2][] - afvec[$-1];
	sfvec ~= sqrt(sum(map!(a => a*a)(temp))/float(allFlow[g][g2].length));
      }
      else{
	agvec ~= 0;
	atvec ~= 0;
	afvec ~= 0;
	sgvec ~= 0;
	stvec ~= 0;
	sfvec ~= 0;
      }
    }
    avgProp.rawWrite(avgPComp.compress(join([to!string(g.rep)]~to!(string[])(agvec), "\t")~"\n"));
    avgThru.rawWrite(avgTComp.compress(join([to!string(g.rep)]~to!(string[])(atvec), "\t")~"\n"));
    avgFlow.rawWrite(avgFComp.compress(join([to!string(g.rep)]~to!(string[])(afvec), "\t")~"\n"));
    stdevProp.rawWrite(stdevPComp.compress(join([to!string(g.rep)]~to!(string[])(sgvec), "\t")~"\n"));
    stdevThru.rawWrite(stdevTComp.compress(join([to!string(g.rep)]~to!(string[])(stvec), "\t")~"\n"));
    stdevFlow.rawWrite(stdevFComp.compress(join([to!string(g.rep)]~to!(string[])(sfvec), "\t")~"\n"));
  }
  
  avgProp.rawWrite(avgPComp.flush());
  avgThru.rawWrite(avgTComp.flush());
  avgFlow.rawWrite(avgFComp.flush());
  stdevProp.rawWrite(stdevPComp.flush());
  stdevThru.rawWrite(stdevTComp.flush());
  stdevFlow.rawWrite(stdevFComp.flush());
  newOut.rawWrite(newComp.flush());
  statsOut.rawWrite(statsComp.flush());
  flowOut.rawWrite(flowComp.flush());
  thruOut.rawWrite(thruComp.flush());
  propOut.rawWrite(propComp.flush());
  bigProp.rawWrite(bigPComp.flush());
  bigThru.rawWrite(bigTComp.flush());
  bigFlow.rawWrite(bigFComp.flush());
  pathsOut.rawWrite(pathsComp.flush());
  return true;

}

class Cell{
  immutable int[2] loc;
  immutable int gen;
  immutable int age;
  immutable double eng;
  immutable double engp;
  double engn;
  long fiti;
  int deadi;
  immutable int gcount;
  Gene[string] genes;
  Network[] nets;
  Tuple!(Gene, double[2])[] orderedExpr;
  double[608] reqs = (0.0).repeat(608).array;
  double[608] ireqs = (0.0).repeat(608).array;
  double[608] ereqs = (0.0).repeat(608).array;
  double[] engTraj;
  double[] conTraj;
  double[int] fitCons;
  bool[] infTraj;
  RedBlackTree!Gene allGenes;
  
  this(int[2] loc, int gen, int age, double eng, double engp, Gene[string] genes, int gcount, Tuple!(Gene, double[2])[] orderedExpr){
    this.loc = loc;
    this.gen = gen;
    this.age = age;
    this.eng = eng;
    this.engp = engp;
    this.genes = genes;
    this.gcount = gcount;
    this.orderedExpr = orderedExpr;
    this.allGenes = new RedBlackTree!Gene();
    auto genNetOut = Network.genNets(new RedBlackTree!Gene(genes.values));    
    this.nets = genNetOut;
    foreach(Network n ; nets){
      foreach(k ; 0..n.reqs.length){
	reqs[k] += n.reqs[k];
      }
      foreach(k ; 0..n.ireqs.length){
	ireqs[k] += n.ireqs[k];
      }
      foreach(k ; 0..n.ereqs.length){
	ereqs[k] += n.ereqs[k];
      }
    }

    foreach(Network n ; nets){
      n.ownerEng = this.engn;
      n.fillNodeStats();
      allGenes.insert(n.genes[]);
    }
  }
}

class Network{
  static int netCount = 0;
  Gene dfsSource;
  RedBlackTree!Gene genes;
  double[608] prods = (0.0).repeat(608).array;
  double[608] reqs = (0.0).repeat(608).array;
  double[608] ireqs = (0.0).repeat(608).array;
  double[608] ereqs = (0.0).repeat(608).array;
  Gene[][608] genP;
  Gene[][608] genN;
  RedBlackTree!Gene pathHeads;
  double fitContr = 0;
  // id will be harder to calculate if we ever want to run this in parallel...
  // but I don't think that will be necessary
  immutable ulong id;
  bool notSet = true;
  int numPaths = 0;
  double engCont = 0;
  double[] engTraj;
  double[] conTraj;
  double ownerEng;
  double netProd;
  double netCost;
  double peakFit;
  double peakProd;
  double peakCost;
  uint peakIter;
  double[int] finalCons;
  bool[] infTraj;
  int[Gene][Gene] edges;
  
  this(Gene first){
    genes = new RedBlackTree!Gene();
    pathHeads = new RedBlackTree!Gene();
    update(first);
    id = netCount;
    netCount++;
  }

  this(ulong id){
    genes = new RedBlackTree!Gene();
    pathHeads = new RedBlackTree!Gene();
    this.id = id;
  }
  
  this(){
    genes = new RedBlackTree!Gene();
    pathHeads = new RedBlackTree!Gene();
    id = netCount;
    netCount++;
  }

  this(RedBlackTree!Gene gs){
    id = netCount;
    netCount++;
    pathHeads = new RedBlackTree!Gene();
    genes = new RedBlackTree!Gene();
    foreach(g ; gs){
      update(g);
    }
  }

  override int opCmp(Object o){
    Network other = to!Network(o);
    if(this.fitContr < other.fitContr)
      return -1;
    else if(this.fitContr > other.fitContr)
      return 1;
    if(this.id < other.id)
      return -1;
    else if(this.id > other.id)
      return 1;
    else
      return 0;
  }

  void update(Gene newG){
    assert(!(newG in genes));
    genes.insert(newG);
    fitContr += newG.engG;
    // it might be possible to optimize this to avoid branch predictions, but it's currently
    // pretty fast. So... if there's some reason to optimize in the future this might be
    // worth looking at.
    if(newG.reactType == 'r' || newG.reactType == 'i'){
      prods[newG.outs[0]] += newG.throughput;
      genP[newG.outs[0]]~=newG;
      if(newG.reactType == 'i'){
	ireqs[newG.outs[0]] += reduce!((a, b) => a + b)(0.0, newG.expr);
      }
    }
    if(newG.reactType == 'r'){
      reqs[newG.ins[0]] += reduce!((a, b) => a + b)(0.0, newG.expr);
      genN[newG.ins[0]]~=newG;
    }
    if(newG.reactType == 'e'){
      ereqs[newG.ins[0]] += reduce!((a, b) => a + b)(0.0, newG.expr);
      genN[newG.ins[0]]~=newG;
    }
    if(newG.reactType == 'r'){
      prods[newG.outs[1]] += newG.throughput;
      reqs[newG.ins[1]] += reduce!((a, b) => a + b)(0.0, newG.expr);
      if(newG.outs[0] != newG.outs[1]){
	genP[newG.outs[1]]~=newG;
      }
      if(newG.ins[0] != newG.ins[1]){
	genN[newG.ins[1]]~=newG;
      }
    }    
    if(newG.reactType == 'r' || newG.reactType == 'e'){
      foreach(Gene g ; genP[newG.ins[0]]){
	g.oGs.insert(newG);
	newG.iGs.insert(g);
      }
    }
    if(newG.reactType == 'r'){
      foreach(Gene g ; genP[newG.ins[1]]){
	g.oGs.insert(newG);
	newG.iGs.insert(g);
      }
    }
    if(newG.reactType == 'r' || newG.reactType == 'i'){
      foreach(Gene g ; genN[newG.outs[0]]){
	g.iGs.insert(newG);
	newG.oGs.insert(g);
      }
    }
    if(newG.reactType == 'r'){
      foreach(Gene g; genN[newG.outs[1]]){
	g.iGs.insert(newG);
	newG.oGs.insert(g);
      }
    }
  }

  static Network[] genNets(RedBlackTree!Gene gens, Gene[] ng = null){
    Network[] result;
    RedBlackTree!Gene genes = gens.dup;
    if(ng !is null)
      foreach(g ; ng)
	genes.removeKey(g);
    RedBlackTree!Gene gs = genes.dup;
    Gene[][int] prodsM;
    bool propd = false;
    foreach(g ; genes){
      g.sat = false;
      if(g.reactType == 'i'){
	if(envProds[g.outs[0]] > 0){
	  prodsM[g.outs[0]] ~= g;
	  g.sat = true;
	  propd = true;
	}
	gs.removeKey(g);
      }
    }
    while(propd){
      propd = false;
      auto remList = DList!Gene();
      foreach(g ; gs){
	switch(g.reactType){
	case 'r':
	  if(g.ins[0] in prodsM && g.ins[1] in prodsM){
	    propd = true;
	    g.sat = true;
	    prodsM[g.outs[0]] ~= g;
	    prodsM[g.outs[1]] ~= g;
	    remList.insert(g);
	  }
	  break;
	case 'e':
	  if(g.ins[0] in prodsM){
	    propd = true;
	    g.sat = true;
	    remList.insert(g);
	  }
	  break;
	default:
	  writeln("genNets found an invalid gene? ", g.reactType);
	  assert(0);
	}
      }
      foreach(g ; remList){
	gs.removeKey(g);
      }
    }
    foreach(g ; genes){
      if(g.sat){
	switch(g.reactType){
	case 'i':
	  break;
	case 'e':
	  g.iGs.insert(prodsM[g.ins[0]]);
	  foreach(ig ; g.iGs){
	    ig.oGs.insert(g);
	  }
	  break;
	case 'r':
	  g.iGs.insert(prodsM[g.ins[0]]);
	  g.iGs.insert(prodsM[g.ins[1]]);
	  foreach(ig ; g.iGs){
	    ig.oGs.insert(g);
	  }
	  break;
	default:
	  writeln("genNets found an invalid gene?");
	  assert(0);
	}
      }
    }
    RedBlackTree!Gene imports = new RedBlackTree!Gene();
    foreach(g ; genes){
      if(g.reactType == 'i' && envProds[g.outs[0]] > 0)
	imports.insert(g);
    }
    Network[] nets = bfsNets(imports);
    return nets;
  }

  // this is going to look very similar to fillNodeStats below
  // but it's purpose is a bit different and re-writing it helps
  // with the reasoning, rather than folding it into fillNodeStats
  void communityDetect(){
    auto pHeads = new RedBlackTree!Gene();
    Gene[] remainingGenes = genes.array;
    while(remainingGenes.length > 0){
      foreach(Gene g ; ){
	if(g.reactType != 'i')
	  continue;
	pHeads.insert(g);
	numPaths += bfs(g);      
      }
    }      
  }
  
  void fillNodeStats(){
    //writeln("Beginning fillNodeStats");
    //writeln(genes.length, " genes");
    auto timeStart = Clock.currTime();
    int count = 0;
    foreach(Gene g ; genes){
      count++;
      //write("\r", count);
      stdout.flush();
      g.engRemImp = g.engT;
      g.dfsSearch = true;
      dfsSource = g;
      // so that the initial dfs call doesn't result in
      // a gene passing throughput to itself immediately
      g.thruFrom[g] = -1*g.throughput;
      dfs(g, g.throughput);
      g.dfsSearch = false;      
      
      // only calculating shortest paths starting from imports
      if(g.reactType != 'i')
	continue;
      pathHeads.insert(g);
      numPaths += bfs(g);
    }
    //writeln("\nbfs/dfsPaths time: ", Clock.currTime() - timeStart);
    foreach(Gene g ; genes){
      calcImportance(g);
    }
    // duplicating iGs and oGs doesn't seem to work properly, so to reset network
    // properties, let's just regenerate it
    genNets(genes);
    timeStart = Clock.currTime();
    foreach(Gene g ; genes){
      g.calcPathStats();
    }
    //writeln("calcPathStats time: ", Clock.currTime - timeStart);
    timeStart = Clock.currTime();
    foreach(Gene g ; genes){
      auto sumIn = 0.0;
      
      foreach(ig ; g.iGs){
	if(ig !in g.thruFrom)
	  continue;
	sumIn += g.thruFrom[ig];
      }
      foreach(kv ; g.thruFrom.byKeyValue()){
	g.propFrom[kv.key] = (sumIn == 0)? 0 : kv.value/sumIn;
      }
      g.thrR = min(sumIn, g.throughput);
      if(g.reactType == 'i')
	g.thrR = g.throughput;
    }
  }

  void calcImportance(Gene g){
    if((g.paths.length == 1 && g.paths[0].length == 1) || g.setRemImp)
      return;

    // this may be a terrible way to go about it, but it's interesting to know
    // whether node removal results in the splitting of networks.
    foreach(gt ; genes){
      gt.iGs.clear();
      gt.oGs.clear();
    }
    auto splitNets = genNets(genes, [g]);
    // fix all this
    g.netSplitImp = splitNets.length;
    g.knockoutRemImp = genes.length - reduce!((a, b) => a + b.genes.length)(0L, splitNets);
    g.engRemImp = engCont - reduce!((a, b) => a + b.engCont)(0.0, splitNets);
    g.pIterImp = -1;
  }

  static Network[] bfsNets(RedBlackTree!Gene genes){
    auto visitedNodes = new RedBlackTree!Gene();
    auto pathTails = new RedBlackTree!Gene();
    auto nextTails = new RedBlackTree!Gene();
    Network[] results;
    foreach(g ; genes){
      if(g in visitedNodes)
	continue;
      pathTails.clear();
      pathTails.insert(g);
      Network newNet = new Network();//results.length);
      while(!pathTails.empty){
	nextTails.clear();
	foreach(fg ; pathTails)
	  visitedNodes.insert(fg);
	foreach(fg ; pathTails){
	  newNet.update(fg);
	  foreach(oG ; fg.oGs){
	    if(oG.sat && !(oG in visitedNodes)){
	      nextTails.insert(oG);
	    }
	  }
	  foreach(iG ; fg.iGs){
	    if(iG.sat && !(iG in visitedNodes)){
	      nextTails.insert(iG);
	    }
	  }
	}
	pathTails = nextTails.dup;
      }
      results~=newNet;
    }
    return results;
  }


  void dfs(Gene sg, double amountPassed){
    int resultCount = 0;
    sg.dfsVisited = true;
    sg.thruFrom[dfsSource] = sg.thruFrom.get(dfsSource, 0) + amountPassed;
    sg.flowFrom[dfsSource] = sg.thruFrom[dfsSource]/dfsSource.throughput;
    if(sg.oGs.length == 0){
      sg.dfsVisited = false;
      return;
    }
    foreach(oG ; sg.oGs){
      double[2] totalReq;
      totalReq[] = -1;
      double pass = 0;
      double thru = (sg.reactType == 'i')? 1 : 0.5;
      switch(oG.reactType){
      case 'r':
	if(sg.outs[0] ==  oG.ins[0] || sg.outs[1] == oG.ins[0]){	
	  double req = reqs[oG.ins[0]];
	  pass += min(amountPassed*thru, req)*min(sg.throughput/req, 1);
	}
	if(sg.outs[0] ==  oG.ins[1] || sg.outs[1] == oG.ins[1]){
	  double req = reqs[oG.ins[1]];
	  pass += min(amountPassed*thru, req)*min(sg.throughput/req, 1);
	}
	break;
      case 'e':
	double ereq = min(max(prods[oG.ins[0]]-reqs[oG.ins[0]], 0), ereqs[oG.ins[0]]);
	if(ereq == 0)
	  break;
	pass = min(amountPassed*thru, ereq) * min(sg.throughput/ereq, 1);
	if(sg.outs[0] == sg.outs[1])
	  pass *= 2;
	break;
      default: // downstream genes should always be reactions or imports
	writeln(sg, "has an oG of ", oG.reactType, "?");
	assert(oG.reactType == 'r' || oG.reactType == 'e');
      }
      if(oG.dfsVisited){
	sg.loopsTo.insert(oG);
	if(oG == dfsSource){
	  dfsSource.thruFrom[dfsSource] += pass;
	  dfsSource.flowFrom[dfsSource] = dfsSource.thruFrom[dfsSource]/dfsSource.throughput;
	}
	continue;
      }
      dfs(oG, pass);
    }
    sg.dfsVisited = false;
    return;
  }
  
  // May want to make a duplicate function which takes
  // additional gene(s) as argument(s) and calculates
  // paths sans it/them
  int bfs(Gene sg){
    auto pathTails = new RedBlackTree!Gene();
    auto endNodes = new RedBlackTree!Gene();
    auto visitedNodes = new RedBlackTree!Gene();
    pathTails.insert(sg);
    sg.visited = true;
    visitedNodes.insert(sg);
    DList!(Gene[]) shortestPaths;
    DList!(Gene[]) paths;
    int pathNum = 0;
    shortestPaths.insert([sg]);
    while(!pathTails.empty){
      auto nextTails = new RedBlackTree!Gene();
      DList!(Gene[]) nextPaths;
      foreach(Gene fg ; pathTails){
	bool oGunsat = true;
	foreach(oG; fg.oGs){
	  if(oG.sat && !oG.visited){
	    oGunsat = false;
	    break;
	  }
	}	
	if(oGunsat || fg.oGs.empty){
	  endNodes.insert(fg);
	}
	foreach(Gene[] path ; shortestPaths){
	  if(path[$-1].s == fg.s){
	    foreach(Gene g ; fg.oGs){
	      if(!g.visited){
		nextPaths.insert(path~g);
		nextTails.insert(g);
	      }
	    }
	  }
	  if(fg in endNodes){
	    pathNum++;
	    if(path[$-1].s == fg.s){
	      foreach(g ; path){
		g.paths~=path;
	      }
	    }
	  }
	}
      }    
      foreach(Gene ng ; nextTails){
	ng.visited = true;
	visitedNodes.insert(ng);
      }
      shortestPaths = nextPaths;      

      pathTails = nextTails;	
    }


    foreach(Gene vg ; visitedNodes)
      vg.visited=false;

    return pathNum;
  }
}


class Gene{
  char reactType;
  double[] expr;
  double[] aff;
  double[] thrus;
  double throughput = -1;
  double thrT = -1;
  double thrR = -1;
  double engG = 0;
  double engB = 0;
  double engT = 0;
  int[2] ins;
  int[2] outs;
  long spec = -1;
  double[2][Gene] conts;
  double[Gene] flowFrom;
  double[Gene] thruFrom;
  double[Gene] propFrom;
  ulong[Gene] splitCount;
  bool contSet = false;
  string[][] sanityList;
  RedBlackTree!Gene iGs;
  RedBlackTree!Gene oGs;
  RedBlackTree!Gene oiGs;
  RedBlackTree!Gene ooGs;
  RedBlackTree!Gene loopsTo;
  string s = "";
  string rep = "";
  int count = 1;
  bool visited;
  Gene[][] paths;
  ulong[] pathinds;
  DList!(Gene[]) dfspaths;
  uint pathma;
  uint pathmi;
  double patha;
  double pathv;
  double engRemImp = 0;
  double engPImp = 0;
  double engCImp = 0;
  double pIterImp = 0;
  double tempEngImp = 0;
  ulong knockoutRemImp = 0;
  ulong netSplitImp = 0;
  bool setRemImp = false;
  bool sat;
  bool tempEssential = false;
  bool dfsVisited = false;
  bool dfsSearch = false;
  int dfspcount = 0;

  
  private this(char reactType, int[2] ins, int[2] outs){
    this.reactType = reactType;
    this.ins[] = ins;
    this.outs[] = outs;
  }

  this(char reactType, int[2] ins, int[2] outs, double expr, double aff){
    this(reactType, ins, outs);
    this.expr ~= expr;
    this.aff ~= aff;
    this.throughput = expr*aff;
    if(reactType == 'i'){
      if(outs[0] >= 53){
	this.throughput = 0;
      }
    }
    this.thrus~=this.throughput;
    if(reactType == 'r'){
      auto oms = [legalMols[0][outs[0]], legalMols[0][outs[1]]];
      auto ims = [legalMols[0][ins[0]], legalMols[0][ins[1]]];
      ulong bo = oms[0].length > oms[1].length? oms[0].length : oms[1].length;
      ulong so = oms[0].length <= oms[1].length? oms[0].length: oms[1].length;
      ulong bi = ims[0].length > ims[1].length? ims[0].length : ims[1].length;
      ulong si = ims[0].length <= ims[1].length? ims[0].length : ims[1].length;
      this.engB = (bo+0.5*so-bi-0.5*si);
      if((ins[0] == outs[0] && ins[1] == outs[1]) || (ins[0] == outs[1] && ins[1] == outs[0]))
	engB = 0;
      this.engG = engB*throughput;
      this.engRemImp = engG;
    }
    this.rep = reactType~":"~to!string(ins)~"->"~to!string(outs);
    this.s = rep~" ("~to!string(count)~")";
    visited = false;
    iGs = new RedBlackTree!Gene();
    oGs = new RedBlackTree!Gene();
    loopsTo = new RedBlackTree!Gene();
    sat = false;
  }

  this(char reactType, int[2] ins, int[2] outs, double[] exprs, double thr, int c){
    this(reactType, ins, outs);
    throughput = thr;
    thrus ~= thr;
    this.expr = exprs;
    //this.expr ~= thr;
    if(reactType == 'r'){
      auto oms = [legalMols[0][outs[0]], legalMols[0][outs[1]]];
      auto ims = [legalMols[0][ins[0]], legalMols[0][ins[1]]];
      ulong bo = oms[0].length > oms[1].length? oms[0].length : oms[1].length;
      ulong so = oms[0].length <= oms[1].length? oms[0].length: oms[1].length;
      ulong bi = ims[0].length > ims[1].length? ims[0].length : ims[1].length;
      ulong si = ims[0].length <= ims[1].length? ims[0].length : ims[1].length;
      this.engB = (bo+0.5*so-bi-0.5*si);
      if((ins[0] == outs[0] && ins[1] == outs[1]) || (ins[0] == outs[1] && ins[1] == outs[0]))
	engB = 0;
      this.engG = engB*throughput;
      this.engRemImp = engG;
    }
    this.rep = reactType~":"~to!string(ins)~"->"~to!string(outs);
    this.s = rep~" ("~to!string(count)~")";
    visited = false;
    iGs = new RedBlackTree!Gene();
    oGs = new RedBlackTree!Gene();
    loopsTo = new RedBlackTree!Gene();
    sat = false;
    count = c;
  }

  this(char reactType, int[2] ins, int[2] outs, double[] exprs, double thr, int c, long s){
    this(reactType, ins, outs, exprs, thr, c);
    this.spec = s;
  }


  override int opCmp(Object o){
    Gene other = to!Gene(o);
    if(this.rep < other.rep){
      return -1;
    }
    else if(this.rep > other.rep){
      return 1;
    }
    else{
      return 0;
    }
  }
  
  override size_t toHash(){
    return typeid(this.rep).getHash(&this.rep);
  }
  
  int opCmp(inout Gene o) const{
    Gene other = cast (Gene)o;
    if(this.reactType != other.reactType){
      if(this.reactType == 'i')
	return -1;      
      else if(this.reactType == 'r' && other.reactType == 'e')
	return -1;
      return 1;
    }
    if(this.rep < other.rep){
      return -1;
    }
    else if(this.rep > other.rep){
      return 1;
    }
    else{
      return 0;
    }
  }


  override bool opEquals(Object o) const{
    auto other = cast(Gene)o;
    return this.rep == other.rep;
  }

  override string toString() const{
    return s;
  }

  void update(double expr, double aff){
    this.expr ~= expr;
    this.aff ~= aff;
    this.count += 1;
    thrus~=expr*aff;
    throughput += expr*aff;
    engG = engB * throughput;
    engRemImp = engG;
    s = rep~" ("~to!string(count)~")";
  }

  void calcPathStats(){
    auto timer = Clock.currTime();
    uint[] locs;
    // for some reason on the cluster std.algorithm doesn't have
    // the "sum" function. Not worth debugging right now so we'll
    // do it manually
    double pathsu = 0;
    pathma = 0;
    pathmi = uint.max;
    foreach(path ; paths){
      foreach(uint i , gene ; path){
	if(gene == this){
	  locs ~= i;
	  pathsu += i;
	  if(i < pathmi)
	    pathmi = i;
	  if(i > pathma)
	    pathma = i;
	  break;
	}
      }
    }
    if(pathma < pathmi)
      pathmi = pathma;
    pathv = 0;
    patha = 0;

    
    
    timer = Clock.currTime();
    /*
    writeln("processing ", dfspcount, " paths");
    int pcount = 0;
    foreach(path ; dfspaths){
      pcount++;
      write("\r", pcount);
      //processDFSPaths(this, path);
    }
    writeln();
    writeln("processDFSPaths: ", Clock.currTime() - timer);
    */
    if(locs.length <= 1)
      return;
    patha = pathsu/locs.length;
    foreach(loc ; locs){
      pathv += pow(loc - patha, 2);
    }
    pathv *= 1.0/(locs.length - 1);
  }

  static void processDFSPaths(Gene source, Gene[] path){
    double percentPassed = 1.0;
    double amountPassed = source.throughput;
    double totalReq = 0;    
    //writeln(1);
    setTotalReq(source, totalReq);
    if(totalReq < amountPassed)
      amountPassed = totalReq;
    /*
    bool foundOne = false;
    foreach(hout ; source.oGs){
      if(canFind(path, hout) || hout in source.loopsTo)
	foundOne = true;
    }
    assert(foundOne || source.oGs.length == 0);
    */
    auto timer = Clock.currTime();
    auto passed = setFlows(source, path[1..$], percentPassed, amountPassed, totalReq);
    timer = Clock.currTime();
    percentPassed = passed[0];
    amountPassed = passed[1];
    //writeln(2);
    auto makeSlices = Clock.currTime();
    auto flowSet = Clock.currTime();
    Duration sliceTime;
    Duration flowTime;
    foreach(loopHead ; path[$-1].loopsTo){
      // RedBlackTrees cast stuff to inout internally which makes comparing an array
      // of objects impossible through operator overloading comparators
      // of those individual objects
      auto visitedSlices = new RedBlackTree!(Gene[], (a, b) =>
					     (function (Gene[] a, Gene[] b) =>
					      to!string(a) < to!string(b)));
      makeSlices = Clock.currTime();
      foreach(lpath ; loopHead.dfspaths){
	auto tind = countUntil(lpath, source);
	if(tind > -1){
	  auto lhind = countUntil(lpath, loopHead);
	  auto loop = lpath[lhind..tind+1];
	  assert(loop[0] == loopHead && loop[$-1] == source);
	  //writeln(3);
	  visitedSlices.insert(loop);
	}	
      }
      sliceTime += Clock.currTime() - makeSlices;
      flowSet = Clock.currTime();
      foreach(slicePath ; visitedSlices){
	setFlows(source, slicePath, percentPassed, amountPassed, totalReq);
      }
      flowTime += Clock.currTime() - flowSet;
    }
    if(path[$-1].loopsTo.length > 1){
      Duration timerTime = Clock.currTime() - timer;
      writeln(path[$-1].loopsTo);      
      writeln(path[$-1].loopsTo.length, " loop paths took: ", timerTime, ". Breakdown - slices: ", sliceTime, "\t flows: ", flowTime);
    }
  }
  
  static Tuple!(double, double) setFlows(Gene source, Gene[] path, double percentPassed, double amountPassed, ref double totalReq){
    if(path.length == 0)
      return Tuple!(double, double)(0,0);

    foreach(gene ; path){
      if(amountPassed > totalReq)
	amountPassed = totalReq;
      percentPassed *= gene.throughput/totalReq;
      amountPassed *= percentPassed;
      assert(!isInfinity(percentPassed));
      
      if(source !in gene.flowFrom){
	gene.flowFrom[source] = 0;
	gene.thruFrom[source] = 0;
      }
      gene.flowFrom[source] += percentPassed;
      gene.thruFrom[source] += amountPassed;
      if(percentPassed > 0){
	assert(gene.reactType != 'i');
      }
      setTotalReq(gene, totalReq);
      /*
      bool foundOne = false;
      foreach(ngene ; gene.oGs){
	if(canFind(path, ngene) || ngene in gene.loopsTo || gene == source)
	  foundOne = true;
      }
      if(!foundOne && gene.oGs.length > 0){
	writeln("source\t", source);
	writeln("path\t", path);
	writeln("gene\t", gene);
	writeln("gene.oGs\t", gene.oGs[]);
      }
      assert(foundOne || gene.oGs.length == 0);
      */
    }
    return Tuple!(double, double)(percentPassed, amountPassed);
  }
  
  static void setTotalReq(Gene g, out double totalReq){
    totalReq = 0;
    foreach(ngene ; g.oGs){
      //totalReq += sum(ngene.expr);
      totalReq += ngene.throughput;
      if(ngene !in g.loopsTo)
	totalReq *= g.splitCount[ngene];
    }
    if(totalReq == 0)
      totalReq = 1;
  }

  bool testSat(){
    bool[2] ws = [ins[0] == -1, ins[1] == -1];
    foreach(g ; iGs){
      if(g.outs[0] == ins[0] || g.outs[1] == ins[0])
	ws[0] = true;
      if(g.outs[0] == ins[1] || g.outs[1] == ins[1])
	ws[1] = true;
      if(ws[0] && ws[1]){
	sat = true;
	return sat;
      }
    }
    sat = ws[0] && ws[1];
    return sat;
  }


  double essential(Gene other){
    if(other == this)
      return 0;
    tempEngImp = 0;
    double result = 0;
    double[2] totals = [0, 0];
    if(!contSet){
      contSet = true;
      foreach(g ; iGs){
	conts[g] = [0, 0];
	if((g.ins[0] == g.outs[0] && g.ins[1] == g.outs[1]) ||
	   (g.ins[1] == g.outs[0] && g.ins[1] == g.outs[0])){	  
	  continue;
	}
	if(g.outs[0] == ins[0] || g.outs[1] == ins[0]){
	  conts[g][0] = g.thrT;
	  totals[0] += g.thrT;
	}
	if(g.outs[1] == ins[0] || g.outs[1] == ins[1]){
	  conts[g][1] = g.thrT;
	  totals[1] += g.thrT;
	}
      }
      foreach(g ; iGs){
	conts[g][0] = conts[g][0] / totals[0];
	conts[g][1] = conts[g][1] / totals[1];
      }
    }
    if(other in iGs){
      result = max(conts[other][0], conts[other][1]);
    }
    return result;
  }
  
  bool essential2(Gene other){
    if(other == this)
      return false;
    tempEssential = true;
    bool result = false;
    if(other in iGs){
      int i = -1;
      if(other.outs[0] == ins[0] || other.outs[1] == ins[0])
	i = 0;
      else
	i = 1;
      foreach(g ; iGs){
	if(g == other || g.tempEssential)
	  continue;
	if(g.outs[0] == ins[i] || other.outs[1] == ins[i]){
	  if(g.essential(other))
	    result = true;
	  else{
	    result = false;
	    tempEssential = false;
	    return result;
	  }
	}
      }
      result = true;
    }
    tempEssential = false;
    return result;
  }
}
