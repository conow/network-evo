import java.io.*;
import java.nio.file.*;
import java.util.zip.*;

class IterationWriter implements Runnable{
    FileOutputStream fos;
    GZIPOutputStream gos;
    ObjectOutputStream oos;
    String fname;
    Object e;
    String baseOut;
    boolean gz;

    IterationWriter(String fname, Object e, String baseOut, boolean gz){
	this.fname = fname;
	this.e = e;
	this.baseOut = baseOut;
	this.gz = gz;
    }

    IterationWriter(int itNum, Object e, String baseOut, boolean gz){
	this(itNum + "", e, baseOut, gz);
    }
    
    public void run(){
	try{
	    fos = new FileOutputStream(baseOut + fname + (gz? ".out.gz" : ".out"));
	    if(gz){
		gos = new GZIPOutputStream(fos, 256000);
		//oos = new ObjectOutputStream(gos);
		//oos.writeObject(e);
		//oos.flush();
		//oos.close();
		Environment.m.writeValue(gos, e);
		gos.flush();
		gos.close();
	    }
	    else{
		fos.write(((String)e).getBytes());
	    }
	    fos.flush();
	    fos.close();
	}
	catch(IOException ioe){
	    ioe.printStackTrace();
	    SimStart.shutDownEverything();
	    System.exit(1);
	}
    }
}
