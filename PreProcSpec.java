import java.math.*;
import java.io.*;
import java.nio.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class PreProcSpec{
    final static Pattern sp = Pattern.compile("\t");
    private static ExecutorService pool;
    // This has a lot of debugging stuff. Since all the reactions are in a file already,
    // only PreProcSpec.reactionsFromFile(String fileName) should be called in Copycat.java
    public static void main(String[] args){
	try{
	    pool = Executors.newFixedThreadPool(Integer.parseInt(System.getenv("NUMCORES")));
	}
	catch(NumberFormatException e){
	    pool = Executors.newFixedThreadPool(4);
	}
	long start = System.nanoTime();
	ConcurrentHashMap<Integer, ArrayList<Long>> result = genSpecs();
	pool.shutdown();
	int prevCount = 0;
	while(!pool.isTerminated()){
	    if(ProductGenerator.count != prevCount && ProductGenerator.count % 1000 == 0){
		prevCount = ProductGenerator.count;
		System.out.println(ProductGenerator.count);
	    }
	}
	System.out.println("Done building reactions! Now time to write...");
	ConcurrentHashMap<Integer, int[]> result2 = ProductGenerator.getResult2();
	System.out.println((System.nanoTime() - start)/1000000000.);
	/*
	  ArrayList<BigInteger> validMols = genLegalMols();
	  ArrayList<BigInteger> validMolCons = genLegalMolCons();
	  for(int i = 0; i < 60; i++){
	  System.out.println(validMols.get(i).toString(4)+"\n"+
	  validMolCons.get(i).toString(4)+"\n");
	  }
	*/
	start = System.nanoTime();
	try{
	    reactionsToFile2(result2, "test.txt");
	}
	catch(IOException e){
	    System.out.println("LOLWUT");
	}
	System.out.println((System.nanoTime() - start)/1000000000.);
	System.out.println(result.size());
	start = System.nanoTime();
	try{
	    int[][] result3 = reactionsFromFile("test.txt");
	    
	    System.out.println((System.nanoTime() - start)/1000000000.);
	    System.out.println(result2.size());
	}
	catch(IOException e){
	    System.out.println("ALL THE SAD THINGS :'(");
	}
    }
    

    public static void reactionsToFile(ConcurrentHashMap<Integer, ArrayList<BigInteger>> reactions, String fileName) throws IOException{
	BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
	String output;
	out.write(""+reactions.size());
	out.newLine();
	for(int i = 0; i < reactions.size(); i++){
	    output = "";
	    for(BigInteger f : reactions.get(i)){
		output += f.toString(4)+"\t";
	    }
	    out.write(output);
	    out.newLine();
	}
	out.flush();
	out.close();
    }

    public static void reactionsToFile2(ConcurrentHashMap<Integer, int[]> reactions, String fileName) throws IOException{
	BufferedWriter out = new BufferedWriter(new FileWriter(fileName));
	String output;
	out.write(""+reactions.size());
	out.newLine();
	for(int i = 0; i < reactions.size(); i++){
	    output = "";
	    for(int f : reactions.get(i)){
		output += f + "\t";
	    }
	    out.write(output);
	    out.newLine();
	}
	out.flush();
	out.close();
    }
	
    public static int[][] reactionsFromFile(String fileName) throws IOException{
	BufferedReader in;	
	in = new BufferedReader(new FileReader(fileName));

	int numLines = Integer.parseInt(in.readLine());
	int[][] result = new int[numLines][4];
	String[] sr;
	int si = 0;
	for(int i = 0; i < numLines; i++){
	    sr = sp.split(in.readLine());
	    for(int j = 0; j < 4; j++){
		si = Integer.parseInt(sr[j]);
		result[i][j] = si;
	    }
	}
	in.close();
	return result;
    }

    // Generates a list of all legal reactions. A reaction is specified
    // by the code for 4 molecules appended together to get a 48 base
    // (or 96 bit) sequence. The first 2 and last 2 are paired to be
    // the factors and products of a reaction
    //
    // (not actually how it works)
    public static ConcurrentHashMap<Integer, ArrayList<Long>> genSpecs(){
	ArrayList<Long> mcList = genLegalMolCons();
	ArrayList<Long> mList = genLegalMols();
	ArrayList<ProductGenerator> pgList = new ArrayList<ProductGenerator>();
	long smaller;
	long smallerMol;
	int smallerLen = -1;
	long bigger;
	long biggerMol;
	int biggerLen = -1;
	int shiftCount = 0;
	
	for(int i = 0; i < mcList.size(); i++){
	    smallerMol = mList.get(i);
	    smaller = mcList.get(i)*10;
	    smallerLen = Long.toString(smaller).length();
	    for(int j = i; j < mcList.size(); j++){
		biggerMol = mList.get(j);
		bigger = mcList.get(j)*10;
		biggerLen = Long.toString(bigger).length();
		pgList.add(new ProductGenerator(biggerMol, smallerMol, bigger, smaller, smallerLen, biggerLen));
	    }
	}
	
	for(ProductGenerator pg : pgList){
	    try{
		pool.execute(pg);
	    }
	    catch(Exception e){}
	}
	return ProductGenerator.getResult();
    }

    // Generates a list of all legal molecules.
    public static ArrayList<Long> genLegalMols(){
	ArrayList<Long> result = new ArrayList<Long>();
	ArrayList<Long> previous = new ArrayList<Long>();
	previous.add(11L);
	previous.add(22L);
	previous.add(33L);
	for(int i = 2; i <= 12; i++){
	    ArrayList<Long> next = new ArrayList<Long>();
	    for(long p : previous){
		int test = (int)(p%10L);
		switch(test){
		case 1:
		    // If the sequence ends in 1, then we can extend it with
		    // 32 and it will still be valid. The next case also applies,
		    // so this case falls through.
		    //next.add(p.shiftLeft(2).add(new BigInteger("22",4)));
		    next.add(p*10+22);
		case 2:
		    // If the sequence ends in 1 or 2, then we can extend it with
		    // 21 or 31 respectively. The only sequence that can end (or begin)
		    // with 3 is 33 so we don't need to worry about that case.
		    //next.add(p.shiftLeft(2).add(new BigInteger("11",4)));
		    next.add(p*10+11);
		    break;
		}
		result.add(p);
	    }
	    previous = next;
	}
	//System.out.println(result.size());
	return result;
    }	

    // Rather than return the sequence for a valid molecule, this returns
    // a sequence that corresponds to the bonds between atoms (bases) of a
    // valid molecule. This is useful for generating the table of legal reactions
    public static ArrayList<Long> genLegalMolCons(){
	ArrayList<Long> result = new ArrayList<Long>();
	ArrayList<Long> previous = new ArrayList<Long>();
	previous.add(1L);
	previous.add(2L);
	previous.add(3L);
	for(int i = 2; i <= 12; i++){
	    ArrayList<Long> next = new ArrayList<Long>();
	    for(long p : previous){
		//int test = p.shiftRight(2).shiftLeft(2).xor(p).intValue();
		int test = (int)(p%10L);
		switch(test){
		case 1:
		    //next.add(p.shiftLeft(2).add(new BigInteger("2",4)));
		    next.add(p*10+2);
		case 2:
		    //next.add(p.shiftLeft(2).add(new BigInteger("1",4)));
		    next.add(p*10+1);
		    break;
		}
		result.add(p);
	    }
	    previous = next;
	}
	return result;
    }

}

// Decided to multithread after I got the rest of the stuff working. It'll be a little wonky in the 
// way everything is organized unless it turns out I'm gonna be extending this a lot, in which case 
// CLEAN THIS UP!
class ProductGenerator implements Runnable{
    static ConcurrentHashMap<Integer, ArrayList<Long>> resultMap = new ConcurrentHashMap<Integer, ArrayList<Long>>();
    static ConcurrentHashMap<Integer, int[]> resultMap2 = new ConcurrentHashMap<Integer, int[]>();
    static ArrayList<Long> legalMols = PreProcSpec.genLegalMols();
    static int count;
    //static ArrayList<BigInteger> legalMolCons = PreProcSpec.genLegalMolCons();
    long bigMol;
    long smallMol;
    long bigCon;
    long smallCon;
    int smallLen;
    int bigLen;
    public ProductGenerator(long bm, long sm, long bc, long sc, int sl, int bl){
	bigMol = bm;
	smallMol = sm;
	bigCon = bc;
	smallCon = sc;
	smallLen = sl;
	bigLen = bl;
    }

    public static ConcurrentHashMap<Integer, ArrayList<Long>> getResult(){
	return resultMap;
    }

    public static ConcurrentHashMap<Integer, int[]> getResult2(){
	return resultMap2;
    }

    public void run(){
	ArrayList<ArrayList<Integer>> conMatches = new ArrayList<ArrayList<Integer>>();
	ArrayList<Integer> newMatch;
	ArrayList<ArrayList<Long>> products = new ArrayList<ArrayList<Long>>();
	ArrayList<Long> newReaction;
	int[] newReactionInds = new int[4];
	for(int i = 0; i < bigLen; i++){
	    long bigmask = bigCon/((long)Math.pow(10L, i))%10L;
	    ArrayList<Integer> theseMatches = new ArrayList<Integer>();
	    for(int j = 0; j < smallLen; j++){
		long smallmask = smallCon/((long)Math.pow(10L, j))%10L;
		if(bigmask == smallmask){
		    theseMatches.add(j);
		}
	    }
	    conMatches.add(theseMatches);
	}		
	//System.out.println(bigCon.toString(4) + "\t" + smallCon.toString(4));
	ArrayList<Integer> lastMatch = new ArrayList<Integer>();
	lastMatch.add(smallLen);
	conMatches.add(lastMatch);
	products = genProds(bigMol, smallMol, conMatches);//, bigCon, smallCon);
	for(ArrayList<Long> p : products){
	    if(p.size() > 2)
		System.out.println("ACK");
	    newReactionInds = new int[4];
	    newReaction = new ArrayList<Long>();
	    newReaction.add(bigMol);
	    newReactionInds[0] = legalMols.indexOf(bigMol);
	    newReaction.add(smallMol);
	    newReactionInds[1] = legalMols.indexOf(smallMol);
	    newReaction.add(p.get(0));
	    newReactionInds[2] = legalMols.indexOf(p.get(0));
	    newReaction.add(p.get(1));
	    newReactionInds[3] = legalMols.indexOf(p.get(1));
	    while(resultMap.putIfAbsent(resultMap.size(), newReaction) != null){}
	    while(resultMap2.putIfAbsent(resultMap2.size(), newReactionInds) != null){} 
	}
	count++;
    }

    public static ArrayList<ArrayList<Long>> genProds(long big, long small, ArrayList<ArrayList<Integer>> matches){
	ArrayList<ArrayList<Long>> result = new ArrayList<ArrayList<Long>>();
	ArrayList<Integer> pair1;
	ArrayList<Integer> pair2;
	ArrayList<Long> prodPair;
	long prod1;
	long prod2;
	for(int i = 1; i < matches.size(); i++){
	    ArrayList<Integer> match = matches.get(i);
	    long bpow = (long)Math.pow(10L, i);
	    for(int sLoc : match){
		prodPair = new ArrayList<Long>();
		long spow = (long)Math.pow(10L, sLoc);
		// get right bits of big
		//prod1 = big.shiftRight(bLoc).shiftLeft(bLoc).xor(big);
		prod1 = big%bpow;
		// get left bits of small
		//prod1 = prod1.add(small.shiftRight(sLoc).shiftLeft(bLoc));
		prod1 += (small - small%spow)/spow*bpow;
		// for now molecules can only be at most 12 bases in length
		if(Long.toString(prod1).length() > 12)
		    continue;
		// get right bits of small
		//prod2 = small.shiftRight(sLoc).shiftLeft(sLoc).xor(small);
		prod2 = small%spow;
		// get left bits of big
		//prod2 = prod2.add(big.shiftRight(bLoc).shiftLeft(sLoc));
		prod2 += (big-big%bpow)/bpow*spow;
		if(Long.toString(prod2).length() > 12)
		    continue;
		
		if(legalMols.indexOf(prod1) == -1 || legalMols.indexOf(prod2) == -1){	
		    System.out.println("sadface :(");
		    System.out.println(big + "\t" + small);
		    System.out.println(prod1 + "\t" + prod2 + "\n");
		    System.exit(1);
		}
		prodPair.add(prod1);
		prodPair.add(prod2);
		if(false && prod1 != big && prod1 != small && prod2 != big && prod2 != small){
		    String out = "";
		    out += big + "\t" + small + "\n";
		    out += prod1 + "\t" + prod2 + "\n";
		    System.out.println(out + "YAY");
		}
		result.add(prodPair);
	    }
	}
	return result;
    }
}
