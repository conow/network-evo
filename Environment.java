import java.util.*;
import java.math.*;
import java.io.*;
import java.nio.*;
import java.nio.file.*;
import java.nio.channels.FileChannel;
import java.util.concurrent.*;
import java.util.stream.IntStream;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.core.type.*;
import java.util.zip.*;

class Environment{
    LiveCell[][] board;
    LiveCell[][] nextBoard;
    // Each cell gets a new Random variable when they change state.
    // With live cells unable to move, it'd make more sense to just
    // have each location have its own random variable that is
    // initialized once, but once/if we allow cells to move, 
    // it'll be simpler to have each cell carry it's own Random
    // with it. And this is all so that we can reproduce a simulation
    // if we need to.
    Random[][] boardRands;
    long[][] boardSeeds; // store seeds rather than randoms, that way multiple cells can mutate 
	
    double[][] engTab;

    TreeMap<Double, LiveCell> breedTree = new TreeMap<Double, LiveCell>();
    
    final double zeroThresh = 1E-4;
    int width;
    int height;
    // the sex flag should probably be a property of individual cells...
    final boolean sex = true;
    ConcurrentSkipListSet<LiveCell> liveCells = new ConcurrentSkipListSet<>(Cell.comp);
    //ArrayList<LiveCell> viableCells = new ArrayList<>();
    HashSet<LiveCell> viableCells = new HashSet<>();
    HashSet<LiveCell> deadCells = new HashSet<>();
    HashSet<LiveCell> infiniteCells = new HashSet<>();
    HashSet<LiveCell> immatureCells = new HashSet<>();
    int viableCount = 0;
    ConcurrentLinkedQueue<Pair> diffuseQueue = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<LiveCell> deadQueue = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<LiveCell> infQueue = new ConcurrentLinkedQueue<>();    
    ConcurrentLinkedQueue<LiveCell> viableQueue = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<LiveCell> immaQueue = new ConcurrentLinkedQueue<>();
    //List<Double> molContent = new ArrayList<>(608);
    double[] molContent = new double[608];
    //List<Double> modContent = new ArrayList<>(608);
    double[] modContent = new double[608];
    //List<Double> maxReplenish;
    double[] maxReplenish = new double[608];
    //List<Double> replenish = new ArrayList<>(608);
    double[] replenish = new double[608];
    static Comparator<int[]> locCompare = new Comparator<int[]>(){
	    public int compare(int[] first, int[] second){
		return (((Integer)first[0]).compareTo(second[0]) != 0)? 
		((Integer)first[0]).compareTo(second[0]) : 
		((Integer)first[1]).compareTo(second[1]);
	    }
	};

    // When a cell is initialized with the Random corresponding to its
    // location, we need to make a new Random for that location for
    // the next cell which is initialized there. We'll be sorting

    // this ArrayList by x and then y coords before using it
    // to initialize new Randoms so that the order is maintained each
    // time the simulation is run with a given seed, even if each cell
    // is initialized/updated in parallel
    ConcurrentSkipListSet<int[]> needsNewRand = new ConcurrentSkipListSet<int[]>(locCompare);
    int mixEnv;
    int lastMix;
    int maxLen;
    int killCount;
    double killGoal;
    double maxEng;
    double minEng;
    int fittestA;
    int fittestNC;
    int numGen;
    int maxGene;
    int maxUsed;
    int maxAge;
    int minAge;
    double maxDist;
    double minDist;
    double sumDist;
    double sumSize;
    int fittestNumImp;
    double sumEng;
    int sumAge;
    int sumNC;
    int boardSize;
    double maxMolSum;
    double maxEngProd;
    double minEngProd;
    double sumEngProd;
    double maxEngUsed;
    double maxEngSurp;
    //double[] molArray = new double[608];
    int burnIn;
    int numMols;
    double propKill;
    final double log3 = Math.log(3);
    static ObjectMapper m = new ObjectMapper();
    double[][] target = new double[2][608];
    double distMax;
    boolean crossPop = false;
    int doaCount = 0;
    boolean infinite = false;
    int infiniteIter = -1;
    int preIterCount = 0;
    int postIterCount = 0;
    int weirdCount = 0;
    Environment(int w, int h){
	init(w, h);	    
	m.configure(SerializationFeature.INDENT_OUTPUT, true);
    }

    private void init(int w, int h){
	board = new LiveCell[h][w];
	nextBoard = new LiveCell[h][w];
	boardRands = new Random[h][w];
	for(int i = 0; i < h; i++){
	    for(int j = 0; j < w; j++){
		boardRands[i][j] = new Random(SimStart.r.nextLong());
	    }
	}
	engTab = new double[h][w];
	width = w;
	height = h;
	boardSize = w*h;
    }
    
    /* If you ever make a copy constructor, make sure you realize that 
     * dealing with keeping reproducibility may be a problem
     * depending on how you use the copy and how the copy is done
     */

    static void alwaysInit(Environment e, double[] p, int isRand, int nm, int bp, double pk) throws IOException{
	e.maxReplenish = Arrays.copyOf(p, 608);
	e.replenish = Arrays.copyOf(p, 608);
	e.mixEnv = isRand;
	e.lastMix = 0;
	e.burnIn = bp;
	e.numMols = nm;
	e.propKill = pk;	
	e.killGoal = Math.ceil(e.propKill*e.height*e.width);
	e.distMax = 0;
	if(!SimStart.useTF){
	    /*
	    for(int i = 0; i < SimStart.targetLength; i++){
		e.target[i] = (SimStart.targetMax-SimStart.targetMin) * SimStart.r.nextDouble() + 
		    SimStart.targetMin;
		if(e.target[i] > 0)
		    e.distMax += Math.pow(e.target[i], 2);
	    }

	    e.distMax = Math.sqrt(e.distMax);
	    */
	    System.err.println("Randomized targets aren't supported at the moment.");
	    System.exit(1);
	}
	else{
	    // this should probably happen in SimStart and not be handled by Environment...
	    BufferedReader in = new BufferedReader(new FileReader(SimStart.targetFile.toString()));
	    String s = "";
	    for(int i = 0; i < 608 && (s = in.readLine()) != null; i++){
		String[] sline = s.trim().split("\t");
		switch(sline.length){
		case 1:
		    e.target[0][i] = Double.parseDouble(sline[0]);
		    e.target[1][i] = Double.parseDouble(sline[0]);
		    break;
		case 2:
		    e.target[0][i] = Double.parseDouble(sline[0]);
		    e.target[1][i] = Double.parseDouble(sline[1]);
		    break;
		default:
		    System.err.println("Something is wrong with the target file!");
		    System.exit(1);
		}
		if(e.target[0][i] > e.target[1][i]){
		    System.err.println(e.target[0][i] + " is larger than " + e.target[1][i] + "! Mol " + i + " in target file");
		    System.exit(1);
		}
		    
		if(e.target[0][i] > 0)
		    e.distMax += Math.pow(Math.max(e.target[0][i], 0), 2);
		    
	    }
	    e.distMax = Math.sqrt(e.distMax);
	}
    }

    static Environment fromDump(Path fname, double[] p, int isRand, int nm, int bp, double pk, boolean crossPop) throws IOException{
	JsonNode snapShot = m.readTree(new GZIPInputStream(new FileInputStream(fname.toFile())));
	int w = snapShot.get("Width").asInt();
	int h = snapShot.get("Height").asInt();
	Environment result = new Environment(w, h);
	int count = 0;
	Iterator<JsonNode> mols = snapShot.get("Concentrations").elements();
	result.crossPop = crossPop;
	while(mols.hasNext()){
	    double thisMol = mols.next().asDouble();
	    if(SimStart.netAnalysis){
		//result.molArray[count] = p.get(count);
		result.molContent[count] = p[count];
		//result.modContent.add(0.0);
		count++;
	    }
	    else{
		//result.molArray[count] = thisMol;
		result.molContent[count] = thisMol;
		//result.modContent.add(0.0);
		count++;
	    }
	}
	alwaysInit(result, p, isRand, nm, bp, pk);
	for(int i = 0; i < result.height; i++){
	    for(int j = 0; j < result.width; j++){
		double mr;
		double rr;
		double tr;
		double dr;
		if(SimStart.netAnalysis){
		    mr = 0.;
		    rr = 0.;
		    tr = 0.;
		    dr = 0.;
		}
		else{
		    mr = SimStart.ptRate;
		    rr = SimStart.rcRate;
		}
		JsonNode individual = snapShot.get("["+i+", "+j+"]");
		Genome g = new Genome(individual.get("Genome").asText(),
				      new Random(SimStart.r.nextLong()), mr, rr);
		result.boardRands[i][j] = new Random(SimStart.r.nextLong());
		int[] loc = {i, j};
		LiveCell newInd = new LiveCell(loc, result, g, mr, rr);
		if(crossPop && false){
		    newInd.energy = individual.get("Energy").asDouble();
		    newInd.bestEnergy = newInd.energy;
		    newInd.lived = individual.get("Age").asInt();
		    if(newInd.lived > newInd.matAge){
			result.viableCells.add(newInd);
		    }
		}
		result.board[i][j] = newInd;
		result.liveCells.add(newInd);
		result.engTab[i][j] = newInd.energy;
	    }
	}	
	return result;
    }

    static Environment crossPops(Environment e1, Environment e2) throws IOException, DeathCountException{
	// for now we'll just use the parameters of the first environment given for this new one
	System.err.println("Crosspops hasn't been set up to deal with infinite cells");
	Environment result = new Environment(e1.width, e1.height);
	alwaysInit(result, SimStart.p, SimStart.randEnv, SimStart.numMols, SimStart.burnIn, SimStart.propKill);
	e1.makeFittest();
	e2.makeFittest();
	result.molContent = e1.molContent;
	result.modContent = e1.modContent;
	//result.molArray = e1.molArray;
	for(int i = 0; i < result.height; i++){
	    for(int j = 0; j < result.width; j++){
		int[] loc = {i, j};
		result.boardRands[i][j] = new Random(SimStart.r.nextLong());
		do{
		    LiveCell lc1 = e1.getParent();
		    LiveCell lc2 = e2.getParent();
		    result.board[i][j] = new LiveCell(loc, result, lc1, lc2);
		    result.board[i][j].call();
		}while(result.board[i][j].doa);
		result.liveCells.add(result.board[i][j]);
		result.engTab[i][j] = result.board[i][j].energy;
	    }
	}
	return result;
    }

    int addInds(ArrayList<LiveCell> newInds){
	if(height > 1){
	    System.err.println("Can't update multi-dimensional environments right now");
	    System.exit(1);
	}
	height = 1;
	width = newInds.size();
	for(int i = 0; i < newInds.size(); i++){
	    board[0][i] = newInds.get(i);
	    engTab[0][i] = newInds.get(i).energy;	    
	}
	liveCells.addAll(newInds);
	return width;
    }
    
    void makeFittest() throws IOException, DeathCountException{
	propKill = 0;
	LinkedBlockingQueue<Future<LiveCell>> runningTasks = new LinkedBlockingQueue<>(width*height);
	for(int i = 0; i < 50; i++){
	    System.out.println(i);
	    for(LiveCell lc : liveCells){
		if(lc.lived < 50)
		    runningTasks.add(SimStart.pool.submit(lc));
		else
		    nextBoard[lc.getLoc()[0]][lc.getLoc()[1]] = lc;
	    }
	    while(runningTasks.size() > 0){
		for(Future<LiveCell> fu : runningTasks){
		    if(fu.isDone()){
			try{
			    fu.get();
			}
			catch(Exception ex){
			    ex.getCause().printStackTrace();
			    System.exit(1);
			}
			diffuse();
			categorize();
			runningTasks.remove(fu);
		    }
		}
	    }
	    try{
		diffuse();
		categorize();
		update();
	    }
	    catch(Exception ex){
		SimStart.output.shutdown();
		SimStart.pool.shutdown();
		throw(ex);
	    }
	}
	makeBreedTree(liveCells);
    }

    void run(int numI) throws IOException, DeathCountException{	
	Long start = System.nanoTime();
	Writer f = SimStart.f;
	Future<LiveCell> fu;
	while(((infinite)? postIterCount : preIterCount) < numI){
	    SimStart.iternum = (infinite)? postIterCount : preIterCount;
	    long parPart = System.nanoTime();
	    for(LiveCell lc : liveCells){
		if(SimStart.numCores > 0){
		    SimStart.compS.submit(lc);
		    SimStart.poolRemaining++;
		}
		else{
		    lc.call();
		    diffuse();
		    categorize();		    
		}
	    }
	    while(SimStart.poolRemaining> 0){
		try{
		    fu = SimStart.compS.take();				
		    fu.get();
		    SimStart.poolRemaining--;
		}
		catch(Exception ex){
		    ex.getCause().printStackTrace();
		    SimStart.shutDownEverything();
		    System.exit(1);
		}		
		diffuse();
		categorize();
	    }

	    try{
		long envTest = System.nanoTime();
		diffuse();
		categorize();
		update();		
		if(preIterCount > 0)
		    f.write(sumString() + "\n");
		
		if(!infinite && (SimStart.dumpOut || SimStart.lineOut) && preIterCount % SimStart.iterout == 0){
		    SimStart.output.execute(new IterationWriter(preIterCount, dumpMapString(), SimStart.baseDumpOut+"/pre/", true));
		}
		else if(infinite && (SimStart.dumpOut || SimStart.lineOut) && postIterCount % SimStart.iterout == 0){
		    SimStart.output.execute(new IterationWriter(postIterCount, dumpMapString(), SimStart.baseDumpOut+"/post/", true));
		}
		
		if(doaCount == liveCells.size() && preIterCount > SimStart.matAge){
		    break;
		}
	    }
	    catch(Exception ex){
		SimStart.shutDownEverything();
		throw(ex);
	    }

	    if(infinite)
		postIterCount++;
	    else
		preIterCount++;
	}
	f.write((System.nanoTime() - start)/1000000000.+"\n");
	if(SimStart.netAnalysis){
	    try{
		SimStart.output.execute(new IterationWriter(numI, dumpMapString(), SimStart.baseNetOut, true));
		makeFittest();
		SimStart.output.execute(new IterationWriter("final", dumpMapString(), SimStart.baseNetOut, true));
	    }
	    catch(Exception ex){
		ex.getCause().printStackTrace();
		SimStart.shutDownEverything();
		System.err.println("Exiting");
		System.exit(1);
	    }
	}
    }
    
    void runWithP(int mR, double rT) throws IOException, DeathCountException{
	propKill = 0;
	double oldVal = replenish[mR];
	replenish[mR] *= rT;
	for(LiveCell lc : liveCells){
	    lc.reinit();
	}
	LinkedBlockingQueue<Future<LiveCell>> runningTasks = new LinkedBlockingQueue<>(width*height);
	for(int i = 0; i < SimStart.maxIter; i++){
	    System.out.println(i);
	    for(LiveCell lc : liveCells){
		if(lc.lived < SimStart.maxIter)
		    runningTasks.add(SimStart.pool.submit(lc));
		else
		    nextBoard[lc.getLoc()[0]][lc.getLoc()[1]] = lc;
	    }
	    while(runningTasks.size() > 0){
		for(Future<LiveCell> fu : runningTasks){
		    if(fu.isDone()){
			try{
			    fu.get();
			}
			catch(Exception ex){
			    ex.getCause().printStackTrace();
			    System.exit(1);
			}
			runningTasks.remove(fu);
		    }
		    diffuse();
		    categorize();
		}
	    }
	    try{
		update();
	    }
	    catch(Exception ex){
		SimStart.output.shutdown();
		SimStart.pool.shutdown();
		throw(ex);
	    }
	}	
	SimStart.output.execute(new IterationWriter(SimStart.maxIter, dumpMapString(), SimStart.baseNetOut + "/" + mR + "_" + rT + "_", true));
	BufferedWriter f = new BufferedWriter(new FileWriter(SimStart.baseNetOut + "/" + mR + "_" + rT + "_cellTrajectories"));
	f.write(cellTrajString());
	f.flush();
	f.close();
	replenish[mR] = oldVal;

    }
    
    LiveCell getParent(){
	double intSelect = SimStart.r.nextDouble();
	LiveCell result = breedTree.ceilingEntry(intSelect).getValue();
	return result;
    }

    void cellListInit(double[] p, int nm, int isRand, int bp) throws IOException{
	alwaysInit(this, p, isRand, nm, bp, 0.0);
	for(int i = 0; i < 608; i++){
	    molContent[i] = replenish[i];
	    modContent[i] = 0.0;
	}
    }
	
    void standardInit(Genome g, double[] p, double ptRate, double rcRate, 
		      int isRand, int nm, int bp, double pk) throws IOException{
	alwaysInit(this, p, isRand, nm, bp, pk);
	for(int i = 0; i < 608; i++){
	    molContent[i] = replenish[i];
	    modContent[i] = 0.0;
	}

	for(int i = 0; i < height; i++){
	    for(int j = 0; j < width; j++){
		boardRands[i][j] = new Random(SimStart.r.nextLong());
		int[] loc = {i, j};
		board[i][j] = new LiveCell(loc, this, g, ptRate, rcRate);
		liveCells.add(board[i][j]);
	    }
	}
	for(LiveCell lc : liveCells){
	    engTab[lc.loc[0]][lc.loc[1]] = lc.energy;
	}
    }


    void testInit(double[] p, int isRand){
	maxReplenish = Arrays.copyOf(p, 608);
	replenish = new double[608];
	mixEnv = isRand;
	lastMix = 0;
	for(int i = 0; i < 608; i++){
	    molContent[i] = replenish[i];
	    modContent[i] = 0.0;
	}
    }

    public String toString(){
	String result = "";
	for(int i = 0; i < height; i++){
	    for(int j = 0; j < width; j++){
		result += board[i][j] + " ";
	    }
	    result += "\n";
	}
	return result;
    }


    public TreeMap<String, Object> dumpMapString() throws JsonProcessingException, IOException, OutOfMemoryError{
	TreeMap<String, Object> result = new TreeMap<String, Object>();
	result.put("Width", width);
	result.put("Height", height);
	result.put("Concentrations", molContent);
	result.put("Provides", replenish);
	for(LiveCell[] ca : board){
	    for(LiveCell c : ca){
		result.put("["+c.getLoc()[0]+", "+c.getLoc()[1]+"]", c.dumpMap());
	    }
	}
	return result;
    }

    public String bornMapString() throws JsonProcessingException{
	TreeMap<String, Object> result = new TreeMap<>();
	result.put("Width", width);
	result.put("Height", height);
	result.put("Concentrations", molContent);
	for(LiveCell[] ca : board){
	    for(LiveCell c : ca){
		if(!c.doa && c.lived == 1)
		    result.put("["+c.getLoc()[0]+", "+c.getLoc()[1]+"]", c.dumpMap());
	    }
	}
	return m.writeValueAsString(result);
    }

    public String molString(){
	StringBuilder temp = new StringBuilder();
	for(int mol = 0; mol < 608; mol++){
	    temp.append(mol + "\t" + molContent[mol] + "\n");
	}
	return temp.toString();
    }
    
    public String lineString(){
	StringBuilder result = new StringBuilder();
	for(LiveCell c : liveCells){
	    if(c.lived == 1 && !c.doa){
		result.append(c.loc[0] + "," + c.loc[1] + "\t" +
			      c.pLocs[0][0] + "," + c.pLocs[0][1] + "," + c.pIterBorn[0] + "," + "\t" +
			      c.pLocs[1][0] + "," + c.pLocs[1][1] + "," + c.pIterBorn[1] + "," + "\n");		
	    }
	}
	return result.toString();
    }

    public String cellTrajString(){
	// if you run this before a simulation is done youre going to have a bad time
	StringBuilder result = new StringBuilder();
	for(int i = 0; i < SimStart.maxIter; i++){
	    for(LiveCell lc : liveCells){
		result.append(lc.fitDist.get(i) + "\t");
	    }
	    result = result.deleteCharAt(result.length() -1);
	    result.append("\n");
	}
	return result.toString();
    }

    public Map<String, Object> lineDumpMap(){
	TreeMap<String, Object> result = new TreeMap<String, Object>();
	result.put("Width", width);
	result.put("Height", height);
	result.put("Concentrations", molContent);
	for(LiveCell[] ca : board){
	    for(LiveCell c : ca){
		result.put("["+c.getLoc()[0]+", "+c.getLoc()[1]+"]", c.dumpMap());
	    }
	}
	return result;
    }

    public String sumString(){
	String result = String.format("%d\t%d\t%d\t|\t"+
				      "%.2f\t%.2f\t%.2f\t|\t"+
				      "%.2f\t%.2f\t%.2f\t|\t"+
				      "%d\t%.2f\t%d\t|\t"+
				      "%d\t%d\t%d\t%.2f\t|\t"+
				      "%d\t%d\t|\t"+
				      "%.2f\t%5.2f\t%.2f",
				      SimStart.iternum + infiniteIter, SimStart.iternum, maxLen,
				      maxEng, minEng, sumEng/sumSize,
				      maxEngProd, minEngProd, sumEngProd/sumSize,
				      viableCount, maxMolSum, fittestNumImp,
				      fittestA, minAge, maxAge, sumAge/sumSize,
				      doaCount, killCount,
				      maxDist, minDist, sumDist/sumSize);
	return result;
    }

    public String statString(){
	StringBuilder result = new StringBuilder();
	result.append("Loc\tAge\tGeneration\tGenesUsed\tEnergy\tEnergyProduced\tEnergyUsed\tEnergySurp\tChromLengths");
	for(Cell[] ca : board){
	    for(Cell c : ca){
		if(c instanceof LiveCell){
		    LiveCell lc = (LiveCell) c;
		    result.append("\n" + lc.sString());
		}
	    }
	}
	return result.toString();
    }

    // in order to avoid concurrent modification issues, cells add their modifications to
    // global concentrations to a blocking queue, which this method then takes values from
    // to update global concentrations in a serial manner. this should hopefully resolve
    // some issues with consistency
    void diffuse(){
	while(diffuseQueue.peek() != null){
	    Pair pair = diffuseQueue.poll();
	    int mol = pair.left;
	    modContent[mol] +=  pair.right;
	}
    }

    void categorize(){
	while(deadQueue.peek() != null){
	    if(deadCells.size() < killGoal)
		deadCells.add(deadQueue.poll());
	    else
		deadQueue.poll();
	}
	while(infQueue.peek() != null){
	    infiniteCells.add(infQueue.poll());
	}
	while(viableQueue.peek() != null){
	    viableCells.add(viableQueue.poll());
	}
	while(immaQueue.peek() != null){
	    immatureCells.add(immaQueue.poll());
	}
    }

    void update() throws IOException, DeathCountException{
	long start = System.nanoTime();
	double div = 1000000000.;
	ArrayList<Long> checks = new ArrayList<>();	
	Arrays.setAll(molContent, i -> Math.max(molContent[i] + modContent[i]/boardSize, replenish[i]));
	Arrays.fill(modContent, 0.0);
	if(mixEnv > 0 && SimStart.iternum > burnIn){
	    if(lastMix == mixEnv){
		lastMix = 0;
		int mol = SimStart.r.nextInt(numMols);
		replenish[mol] = SimStart.r.nextDouble() * maxReplenish[mol];
	    }
	    else
		lastMix++;
	}
	long finishRep = System.nanoTime();

	board = nextBoard;
	nextBoard = new LiveCell[height][width];
	long clearStuff = System.nanoTime();

	doaCount = deadCells.size();
	double needKill = killGoal - doaCount;
	int checkC = 0;
	while(viableCells.size() > needKill && deadCells.size() < killGoal){
	    HashSet<LiveCell> tempCells = killFrom(viableCells, needKill, true);
	    viableCells.removeAll(tempCells);
	    needKill -= tempCells.size();
	    deadCells.addAll(tempCells);
	    checkC++;
	    if(needKill + deadCells.size() != killGoal){
		throw new DeathCountException(killGoal, needKill + deadCells.size());
	    }
	}
	//System.err.println("Ran " + checkC);
	checks.add(System.nanoTime());
	// if after the previous process, we still need to kill things and the total number
	// of non-infinite cells is less than that, we just kill all viable cells off before
	// moving on to infinite cells
	if(needKill > 0 && viableCells.size() <= needKill){
	    deadCells.addAll(viableCells);
	    needKill -= viableCells.size();
	    viableCells.clear();
	    if(needKill + deadCells.size() != killGoal){
		throw new DeathCountException(killGoal, needKill + deadCells.size());
	    }
	}		
	checks.add(System.nanoTime());
	if(infiniteCells.size() > 0){
	    if(!infinite){
		infinite = true;
		infiniteIter = SimStart.iternum;
	    }

	    // Now we start killing infinite cells if we still have a quota to fulfill
	    while(infiniteCells.size() > needKill && deadCells.size() < killGoal){
		try{
		    HashSet<LiveCell> tempCells = killFrom(infiniteCells, needKill);
		    infiniteCells.removeAll(tempCells);
		    needKill -= tempCells.size();
		    deadCells.addAll(tempCells);
		    if(needKill + deadCells.size() != killGoal){
			throw new DeathCountException(killGoal, needKill + deadCells.size());
		    }
		}
		catch(ArrayIndexOutOfBoundsException ex){
		    System.err.println(infiniteCells.size());
		    System.err.println(needKill);
		    throw(ex);
		}
	    }
	}
	checks.add(System.nanoTime());
	if(killGoal != deadCells.size()){
	    TreeSet<LiveCell> immatureSet = new TreeSet<>(LiveCell.byEng);
	    immatureSet.addAll(immatureCells);
	    while(deadCells.size() < killGoal && immatureSet.size() > 0){
		LiveCell deadC = immatureSet.pollLast();
		deadCells.add(deadC);
		immatureCells.remove(deadC);
		needKill--;
	    }
	}
	if(killGoal != deadCells.size() && (viableCells.size()+infiniteCells.size()) > needKill){
	    System.err.println(weirdCount);
	    System.err.println(viableCells.size());
	    System.err.println(infiniteCells.size());
	    System.err.println(needKill);
	    System.err.println(liveCells.size());
	    throw new DeathCountException(killGoal, deadCells.size());
	}
	checks.add(System.nanoTime());
	killCount = deadCells.size();
	// Actually kill them.
	for(LiveCell lc : deadCells){
	    lc.killed = true; 
	}
	for(int[] makeNewRand : needsNewRand){
	    long newSeed = SimStart.r.nextLong();
	    boardRands[makeNewRand[0]][makeNewRand[1]] = new Random(newSeed);
	}
	needsNewRand.clear();

	checks.add(System.nanoTime());
	if(infiniteCells.size() > 0)
	    makeBreedTree(infiniteCells);
	else
	    makeBreedTree(viableCells);
	checks.add(System.nanoTime());
	// this is hacky and I really don't like it
	viableCells.addAll(infiniteCells);
	checks.add(System.nanoTime());
	
	if(true){
	    sumEng = 0;
	    sumAge = 0;
	    maxEng = Double.NEGATIVE_INFINITY;
	    minEng = Double.POSITIVE_INFINITY;
	    maxGene = 0;
	    maxUsed = 0;
	    maxMolSum = 0;
	    maxEngProd = 0;
	    minEngProd = Double.POSITIVE_INFINITY;
	    maxEngUsed = 0;
	    maxEngSurp = 0;
	    maxLen = 0;
	    maxAge = 0;
	    minAge = Integer.MAX_VALUE;
	    sumNC = 0;
	    maxDist = 0;
	    minDist = Double.POSITIVE_INFINITY;
	    sumDist = 0;
	    sumSize = 0;
	    sumEngProd = 0;
	    for(LiveCell tc : viableCells){
		int thisGene = 0;
		int thisUsed = 0;
		double thisMolSum = 0;
		double thisProduced = 0;
		if(tc.lived > tc.matAge){// || true){
		    sumSize++;
		    for(Chromosome chr: tc.gen.gen){
			//thisGene += chr.geneLocLen;
			if(chr.len > maxLen)
			    maxLen = chr.len;
		    }
		    if(tc.energy > maxEng){
			maxEng = tc.energy;
			if(SimStart.selectionDirection > 0 && !infinite){
			    fittestNumImp = 0;
			    fittestA = tc.lived;
			    for(int m = 0; m < 608; m++){
				fittestNumImp += (tc.internalMolContent[m] > 0)? 1 : 0;
			    }
			    fittestNC = tc.numC;
			}
		    }
		    if(tc.lived > maxAge){
			if(!infinite || tc.infinite){
			    maxAge = tc.lived;
			}
		    }		    
		    if(tc.lived < minAge){
			if(!infinite || tc.infinite)
			    minAge = tc.lived;
		    }
		    if(tc.energy < minEng){
			minEng = tc.energy;
			if(SimStart.selectionDirection < 0 && !infinite){
			    fittestNumImp = 0;
			    fittestA = tc.lived;
			    fittestNC = tc.numC;
			    for(int m = 0; m < 608; m++){
				fittestNumImp += (tc.internalMolContent[m] > 0)? 1 : 0;
			    }
			}
		    }
		    if(tc.generation > numGen)
			numGen = tc.generation;
		    if(tc.numGene > maxGene){
			maxGene = tc.numGene;
		    }
		    if(tc.genesUsed > maxUsed){
			maxUsed = tc.genesUsed;
		    }
		    
		    if(tc.molSum > maxMolSum){
			maxMolSum = tc.molSum;
		    }
		    
		    if(tc.energyProd > maxEngProd){
			maxEngProd = tc.energyProd;
			if(SimStart.selectionDirection > 0 && infinite){
			    fittestNumImp = 0;
			    fittestA = tc.lived;
			    fittestNC = tc.numC;
			    for(int m = 0; m < 608; m++){
				fittestNumImp += (tc.internalMolContent[m] > 0)? 1 : 0;
			    }
			}
		    }
		    if(tc.energyProd < minEngProd){
			minEngProd = tc.energyProd;
			if(SimStart.selectionDirection < 0 && infinite){
			    fittestNumImp = 0;
			    fittestA = tc.lived;
			    fittestNC = tc.numC;
			    for(int m = 0; m < 608; m++){
				fittestNumImp += (tc.internalMolContent[m] > 0)? 1 : 0;
			    }
			}
		    }
		    
		    if(tc.energySurp > maxEngSurp){
			maxEngSurp = tc.energySurp;
		    }
		    
		    if(tc.energyUsed > maxEngUsed){
			maxEngUsed = tc.energyUsed;
		    }
		    if(tc.distSum > maxDist){
			maxDist = tc.distSum;			
		    }
		    if(tc.distSum < minDist){
			minDist = tc.distSum;
		    }
		    sumDist += tc.distSum;
		    sumAge += tc.lived;
		    sumEng += tc.energy;
		    sumEngProd += tc.energyProd;
		    sumNC += tc.numC;
		}
	    }
	}
	viableCount = viableCells.size();
	viableCells.clear();
	deadCells.clear();
	infiniteCells.clear();
	immatureCells.clear();
	checks.add(System.nanoTime());
	double totTime = (checks.get(checks.size()-1)-start)/div;
	double molTime = (finishRep - start)/div;
	double clearTime = (clearStuff - finishRep)/div;
	String timeString = "Started:\t" + start/div;
	timeString += "\nMolStuff:\t" + molTime + "\t" + molTime/totTime;
	timeString += "\nClearStuff:\t" + clearTime + "\t" + clearTime/totTime;
	double check0Time = (checks.get(0) -clearStuff)/div;
	timeString += "\nCheck0:\t" + check0Time + "\t" + check0Time/totTime;
	for(int i = 1; i < checks.size(); i++){
	    double thisTime = (checks.get(i) - checks.get(i-1))/div;
	    timeString += "\nCheck" + i + ":\t" + thisTime + "\t" + thisTime/totTime;
	}
	timeString += "\nTotal:\t" + totTime;
	//System.err.println(timeString);
	
    }

    private HashSet<LiveCell> killFrom(Set<LiveCell> pop, double killcount, boolean unused){
	long start = System.nanoTime();
	ArrayList<Long> checks = new ArrayList<>();
	HashSet<LiveCell> deadCells = new HashSet<>();
	LiveCell[] popArray = new LiveCell[pop.size()];
	TreeMap<Double, LiveCell> dieMap = new TreeMap<>();
	double engAvg = 0;
	double engTot = 0;
	int count = 0;
	checks.add(System.nanoTime());
	for(LiveCell lc : pop){
	    engTot += lc.energy;
	    popArray[count] = lc;
	    count++;
	}
	engAvg = engTot/pop.size();
	checks.add(System.nanoTime());
	for(LiveCell lc : popArray){
	    if(dieMap.size() < killcount)
		dieMap.put(SimStart.r.nextDouble()*engAvg*0.1 + lc.energy, lc);
	    else{
		double mod = SimStart.r.nextDouble()*engAvg*0.1 + lc.energy;
		if(mod < dieMap.lastKey()){
		    dieMap.pollLastEntry();
		    dieMap.put(mod, lc);
		}
	    }
	}
	//TreeMap<Double, LiveCell> dieMap = new TreeMap<>(dieMapH);
	checks.add(System.nanoTime());	   
	killCount = Math.min(dieMap.size(), killCount);
	/*
	for(int i = 0; i < killcount; i++){
	    deadCells.add(dieMap.pollFirstEntry().getValue());
	    }*/
	deadCells.addAll(dieMap.values());
	checks.add(System.nanoTime());
	double div = 1000000000.;
	double totTime = (checks.get(checks.size()-1)-start)/div;
	String timeString = "Started:\t" + start/div;
	double check0Time = (checks.get(0) - start)/div;
	timeString += "\nKill0:\t" + check0Time + "\t" + check0Time/totTime;
	for(int i = 1; i < checks.size(); i++){
	    double thisTime = (checks.get(i) - checks.get(i-1))/div;
	    timeString += "\nKill" + i + ":\t" + thisTime + "\t" + thisTime/totTime;
	}
	timeString += "\nTotal:\t" + totTime;
	//System.err.println(timeString);
	return deadCells;
    }
    
    private HashSet<LiveCell> killFrom(Set<LiveCell> pop, double killcount){
	long start = System.nanoTime();
	ArrayList<Long> checks = new ArrayList<>();
	HashSet<LiveCell> deadCells = new HashSet<>();
	TreeMap<Double, LiveCell> dieMap = new TreeMap<>();
	checks.add(System.nanoTime());
	double[] engArry = new double[pop.size()];
	LiveCell[] popArray = new LiveCell[pop.size()];
	popArray = pop.toArray(popArray);
	checks.add(System.nanoTime());
	HashSet<Double> checkKeys = new HashSet<>();
	double ss = SimStart.selectionDirection*SimStart.killSelect;
	double engAvg = 0;
	//double engMax = pop.stream().mapToDouble(LiveCell::getEnergy).max().orElse(0);
	checks.add(System.nanoTime());
	for(int i = 0; i < popArray.length; i++){
	    engArry[i] = popArray[i].energy;
	    //engArry[i] = 1/viableArray[i].distSum;
	    engAvg += engArry[i];
	}
	engAvg /= engArry.length;
	checks.add(System.nanoTime());
	// Selecting which cells should die. Transforming fitness using
	// an exponential function to penelize cells with very small fitness
	// significantly more than those with less low fitness. Note that this 
	// selects 1 out of every 16 such cells with replacement, so if there 
	// are fewer than 1/16th of the total cells with significantly lower 
	// fitness than anything else, some of them may be selected to die more 
	// than once. I'm ok with this.
	double[] dieIntervals = new double[engArry.length];
	
	dieIntervals[0] = Math.exp(ss*(1 - engArry[0]/engAvg));
	double dieTotal = dieIntervals[0];
	// Create weighted intervals
	for(int i = 1; i < engArry.length; i++){
	    dieIntervals[i] = Math.exp(ss*(1 - engArry[i]/engAvg));
	    dieTotal += dieIntervals[i];
	}
	checks.add(System.nanoTime());
	// Compress them to occur in the range [0,1]
	dieIntervals[0] = dieIntervals[0]/dieTotal;
	dieMap.put(dieIntervals[0], popArray[0]);
	for(int i = 1; i < dieIntervals.length; i++){
	    dieIntervals[i] = dieIntervals[i]/dieTotal+dieIntervals[i-1];
	    // if the average is so large that some values don't distinguish themselves
	    // from the previous, those values are small enough that we'll ignore selecting
	    // them for now. The alternative is either overwriting the previous distinct value
	    // (which must be a sizeable portion of the average) with a less fit one (which
	    // obviously is bad) or using a sorted structure to add stuff to dieMap which will
	    // add sorting overhead to all this.
	    if(!checkKeys.contains(dieIntervals[i])){
		dieMap.put(dieIntervals[i], popArray[i]);
		checkKeys.add(dieIntervals[i]);
	    }
	}
	checks.add(System.nanoTime());
	// Select them at random with replacement!
	// Should we even be doing this randomly?
	// Would it be awful to just kill the number of least-fit
	// individuals we want dead?
	for(int i = 0; i < killcount; i++){
	    
	    double intSelect = SimStart.r.nextDouble();
	    
	    double curMax = dieIntervals[0];
	    int selection = 0;
	    LiveCell deadCell = dieMap.ceilingEntry(intSelect).getValue();
	    if(deadCells.contains(deadCell))
		i = i -1;
	    else		
		deadCells.add(dieMap.ceilingEntry(intSelect).getValue());
	    
	    //deadCells.add(dieMap.pollFirstEntry().getValue());
	}
	checks.add(System.nanoTime());
	double div = 1000000000.;
	double totTime = (checks.get(checks.size()-1)-start)/div;
	String timeString = "Started:\t" + start/div;
	double check0Time = (checks.get(0) - start)/div;
	timeString += "\nKill0:\t" + check0Time + "\t" + check0Time/totTime;
	for(int i = 1; i < checks.size(); i++){
	    double thisTime = (checks.get(i) - checks.get(i-1))/div;
	    timeString += "\nKill" + i + ":\t" + thisTime + "\t" + thisTime/totTime;
	}
	timeString += "\nTotal:\t" + totTime;
	//System.err.println(timeString);
	return deadCells;
    }
    
    private void makeBreedTree(Set<LiveCell> pop){
	if(pop.size() == 0){
	    return;
	}
	double engAvg = 0;
	double engTot = 0;
	double engMax = pop.stream().mapToDouble(LiveCell::getEnergy).max().orElse(0);
	int avgCount = 0;
	for(LiveCell lc : pop){
	    engTot += lc.energy;
	    if(lc.killed){
		System.err.println("Killed cell got into makeBreedTree");
		Thread.currentThread().dumpStack();
		SimStart.shutDownEverything();
		System.exit(1);
	    }
	    // If the average energy level after normalization is too low, when we
	    // use the exponential function later on the highest energy individual, we
	    // can end up doing math on infinite numbers. If after normalization, a value
	    // is less than 1/100th of the maximum, It's probably safe to assume that it will never be
	    // selected so we just don't worry about it. This will increase selection for higher fitness
	    // individuals relative to population size.
	    /*
	    if(lc.energy > 0.01)
		avgCount++;
	    */
	}
	
	engAvg = engTot/pop.size();
	double[] breedIntervals = new double[pop.size()];
	breedTree.clear();
	if(pop.size() == 1){
	    breedTree.put(1.0, (LiveCell)pop.toArray()[0]);
	    return;
	}
	double breedTotal = 0;
	double bs = SimStart.selectionDirection*SimStart.repSelect;
	LiveCell[] popArray = pop.toArray(new LiveCell[0]);
	double breedMax = 0;
	for(int i = 0; i < popArray.length; i++){
	    breedIntervals[i] = Math.exp(-bs*(1-(popArray[i].energy)/engAvg));
	    if(breedIntervals[i] > breedMax)
		breedMax = breedIntervals[i];
	}
	for(int i = 0; i < breedIntervals.length; i++){
	    //breedIntervals[i] = breedIntervals[i]/breedMax;
	    breedTotal += breedIntervals[i];
	}

	double expMod = 0;
	int infCount = 1;
	if(Double.isInfinite(breedTotal)){
	    if(!Double.isInfinite(breedMax)){
		breedTotal = 0;
		for(int i = 0; i < breedIntervals.length; i++){
		    if(Double.isInfinite(breedTotal + breedIntervals[i])){
			infCount++;
			breedTotal -= Double.MAX_VALUE;
		    }		
		    breedTotal += breedIntervals[i];
		    if(breedTotal < 0){
			System.err.println("you were wrong");
			System.exit(1);
		    }
		    if(Double.isInfinite(breedTotal)){
			System.err.println("we boned");
			System.err.println(breedIntervals[i] + "\t" + popArray[i].energy + "\t" + engAvg);
			System.exit(1);
		    }
		}
		expMod = -1*Math.log(1.0/(double)(pop.size() * infCount));
		breedMax = 0;
		breedTotal = 0;
		for(int i = 0; i < popArray.length; i++){
		    breedIntervals[i] = Math.exp(-bs*(1-(popArray[i].energy+1)/engAvg)-expMod);
		    if(breedIntervals[i] > breedMax)
			breedMax = breedIntervals[i];
		}
		for(int i = 0; i < breedIntervals.length; i++){
		    breedTotal += breedIntervals[i];
		}
	    }
	    // some individuals are so much more fit than the average that we'll just pretend that they're infinite	    
	    else{
		breedTotal = 0;		
		for(int i = 0; i < breedIntervals.length; i++){
		    if(Double.isInfinite(breedIntervals[i])){
			breedIntervals[i] = popArray[i].energy;
			breedTotal += popArray[i].energy;
		    }
		    else{
			breedIntervals[i] = 0;
		    }
		}
	    }
	}

	

	breedIntervals[0] = breedIntervals[0]/breedTotal;
	breedTree.put(breedIntervals[0], popArray[0]);

	for(int i = 1; i < breedIntervals.length - 1; i++){
	    breedIntervals[i] = breedIntervals[i]/breedTotal + breedIntervals[i-1];
	    breedTree.put(breedIntervals[i], popArray[i]);
	}
	// due to floating point rounding it is possible that the last value is 0.9999999999999999923423 or something like that.
	// this just checks to see if the last interval is "close enough". If it isn't, there is a bug we must fix.
	double lastInterval = breedIntervals[breedIntervals.length - 1]/breedTotal + breedIntervals[breedIntervals.length - 2];
	if(lastInterval < 1 && lastInterval + 0.0000001 >= 1){
	    lastInterval = 1;
	}
	breedIntervals[breedIntervals.length - 1] = lastInterval;
	breedTree.put(lastInterval, popArray[breedIntervals.length-1]);
	if(breedTree.firstKey() == breedTree.lastKey() && breedTree.firstKey() == 0 || breedTree.lastKey() < 1 || breedTree.lastKey() > 1.0001){
	    for(int i = 0; i < breedIntervals.length; i++){
  		double temp = Math.exp(-bs*(1-(popArray[i].energy+1)/engAvg)-expMod);
		double temp2 = Math.exp(-bs*(1-(popArray[i].energy)/engAvg));
		System.err.println(i + "\t" + breedIntervals[i] + "\t" + popArray[i].energy/engMax  + "\t" +  (-bs*(1-(popArray[i].energy)/engAvg)+expMod) + "\t" + temp2+ "\t" + temp + "\t" + temp/breedTotal);
	    }
	    System.err.println("~~~" + pop.size() + "\t" + engAvg + "\t" + engMax + "\t" + expMod + "\t" + breedTotal + "\t" + infCount + "\t" + 1./(infCount * pop.size()) + "\t" + breedTree.lastKey());
	    System.exit(1);
	}
    }
}


class DeathCountException extends Exception{
    DeathCountException(double kg, double nk){
	super(kg + " is goal, but killing " + nk);
    }
}
