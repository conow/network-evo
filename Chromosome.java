import java.util.*;
import java.math.*;
import java.io.*;
import java.util.stream.*;

final class Chromosome{
    byte[][] chrom;
    int len;
    private int[] lens;
    private TreeSet<Integer> geneLocs;
    SortedSet<Integer> geneLocView;
    private double sceRate;
    private boolean dead;
    private int s = 0;
        
    Chromosome(Chromosome c){
	this.chrom = new byte[2][];
	this.chrom[0] = Arrays.copyOf(c.chrom[0], c.chrom[0].length);
	this.chrom[1] = Arrays.copyOf(c.chrom[1], c.chrom[1].length);
	len = c.len;
	lens = new int[]{c.lens[0], c.lens[1]};
	geneLocs = c.getGeneLocs();
	geneLocView = Collections.unmodifiableSortedSet(geneLocs);
	sceRate = c.sceRate;
	dead = c.dead();
    }
    
    Chromosome(String[] s, double scr){
	makeChromB(s);
	lens = new int[]{chrom[0].length*4, chrom[1].length*4};
	len = lens[0] + lens[1];
	geneLocs = findGenes();
	geneLocView = Collections.unmodifiableSortedSet(geneLocs);
	sceRate = scr;
	dead = s[0].length() == 0 || s[1].length() == 0;
    }

    Chromosome(StringBuilder[] sb, double scr, TreeSet<Integer> gLocs){
	StringBuilder[] s = new StringBuilder[]{new StringBuilder(sb[0]), new StringBuilder(sb[1])};
	makeChromB(s);
	lens = new int[]{chrom[0].length*4, chrom[1].length*4};
	len = lens[0] + lens[1];
	geneLocs = new TreeSet<Integer>(gLocs);
	geneLocView = Collections.unmodifiableSortedSet(geneLocs);
	sceRate = scr;
	dead = s[0].length() == 0 || s[1].length() == 0;
    }
    
    Chromosome(StringBuilder[] s, double scr){
	this(new String[]{s[0].toString(), s[1].toString()}, scr);
    }
    
    Chromosome(String s, double scr){
	this(splitChr(s), scr);
    }
    
    private void makeChromB(CharSequence[] s){
	chrom = new byte[][]{new byte[(s[0].length()+3)/4], new byte[(s[1].length()+3)/4]};
	for(int c = 0; c < chrom.length; c++){
	    for(int i = 0; i < s[c].length(); i++){
		char b = s[c].charAt(i);
		switch (b){
		case '1':
		    chrom[c][i/4] += 1 << 2 * (3 - i%4);
		    break;
		case '2':
		    chrom[c][i/4] += 2 << 2 * (3 - i%4);
		    break;
		case '3':
		    chrom[c][i/4] += 3 << 2 * (3 - i%4);
		    break;
		default:
		    break;
		}
	    }
	}
    }
    

    void reinit(double scr, TreeSet<Integer> gLocs){
	len = lens[0] + lens[1];
	geneLocs.clear();
	geneLocs = gLocs;
	geneLocView = Collections.unmodifiableSortedSet(geneLocs);
	sceRate = scr;
	dead = lens[0] == 0 || lens[1] == 0;	
    }


    public TreeSet<Integer> getGeneLocs(){
	return new TreeSet<Integer>(geneLocs);
    }
    

    public StringBuilder[] stringBCopy(){
	StringBuilder[] result = new StringBuilder[2];
	result[0] = new StringBuilder(baString(chrom[0]));
	result[1] = new StringBuilder(baString(chrom[1]));
	return result;
    }

    
    public String toString(){
	return baString(chrom[0]) + '@' + baString(chrom[1]);
    }

    public static String baString(byte[] b){
	StringBuilder result = new StringBuilder();
	for(byte s : b){
	    String t = "000" + Integer.toUnsignedString(s, 4);
	    result.append(t.substring(t.length()-4));
	}
	return result.toString();
    }

    public static String baString(byte b){
	String t = "000" + Integer.toUnsignedString(b, 4);
	return t.substring(t.length()-4);
    }
    
    public static String bsString(BitSet bs, int len){
	StringBuilder result = new StringBuilder();
	for(int i = 0; i < len; i += 2){
	    if(bs.get(i)){
		if(bs.get(i+1))
		    result.append("3");
		else
		    result.append("2");
	    }
	    else{
		if(bs.get(i+1))
		    result.append("1");
		else
		    result.append("0");
	    }
	}
	return result.toString();
    }

    public boolean dead(){
	return dead;
    }

    public int[] getLens(){
	return new int[]{lens[0], lens[1]};
    }
    
    private static String[] splitChr(String in){
	String[] result = new String[2];
	if(in.indexOf('@') > -1){
	    result = in.split("@");
	}
	else{
	    result[0] = in.substring(0, in.length()/2);
	    result[1] = in.substring(in.length()/2);
	}
	if(result.length == 1)
	    result = new String[]{result[0], ""};
	else if(result.length == 0)
	    result = new String[]{"", ""};
	return result;
    }
    
    boolean pointMutate(Random rand, byte[][] chrom, TreeSet<Integer> geneLocs){
	int loc = rand.nextInt(lens[0]+lens[1]);
	int which = (loc < lens[0])? 0 : 1;
	loc = loc - lens[0];

	for(int i = -3; i <= 0; i++){
	    // since this always results in a base change, we know that any gene start site
	    // hit will be disrupted, regardless of what the base is changed to
	    if(geneLocs.remove(loc + i)){
		if(SimStart.verbose)
		    System.err.print("\tremoved " + (loc + i));
	    }
	}

	
	byte toWhich = (byte)(1+rand.nextInt(3));
	int off = (which == 0)? lens[0] : 0;
	toWhich <<= 2*((loc+off)%4);
	chrom[which][(loc+off)/4] ^= toWhich;


	int newGeneLoc = checkLocalLoc(loc+off, chrom, which);
	if(newGeneLoc >= 0){
	    geneLocs.add(newGeneLoc - off);
	    if(SimStart.verbose)
		System.err.print("\tadded " + (newGeneLoc - off));
	}
	if(SimStart.verbose)
	    System.err.println();
	return true;
    }

    public static boolean pointMutate(Random rand, TreeSet<Integer> geneLocs, BitSet[] chromB, int[] lens, byte[][] chrom){
	int loc = rand.nextInt(lens[0]+lens[1]);
	if(SimStart.verbose)
	    System.err.print("mut: " + loc);
	int which = (loc < lens[0])? 0 : 1;
	loc = loc - lens[0];

	for(int i = -3; i <= 0; i++){
	    // since this always results in a base change, we know that any gene start site
	    // hit will be disrupted, regardless of what the base is changed to
	    if(geneLocs.remove(loc + i)){
		if(SimStart.verbose)
		    System.err.print("\tremoved " + (loc + i));
	    }
	}
	
	
	int toWhich = rand.nextInt(3)+1;
	int off = (which == 0)? lens[0] : 0;
	int base = (chromB[which].get((loc+off)*2)? 2 : 0) + (chromB[which].get((loc+off)*2+1)? 1 : 0);
	int mask = ~(3<<(2*(3-(loc+off)%4)));
	int out = ((toWhich+base)%4)<<(2*(3-(loc+off)%4));
	try{
	    chrom[which][(loc+off)/4] = (byte)((chrom[which][(loc+off)/4] & mask) ^ out);
	}
	catch(Exception e){
	    System.err.println("len " + chrom[0].length*4 + ", " + chrom[1].length*4);
	    System.err.println("loc " + loc);
	    System.err.println("off " + off);
	    System.err.println("lo  " + (loc+off));
	    System.err.println("lens " + lens[0] + ", " + lens[1]);
	    SimStart.shutDownEverything();
	    System.exit(1);
	}

	int newGeneLoc = checkLocalLoc(loc+off, chromB, lens[which], which);
	if(newGeneLoc >= 0){
	    geneLocs.add(newGeneLoc - off);
	    if(SimStart.verbose)
		System.err.print("\tadded " + (newGeneLoc - off));
	}
	if(SimStart.verbose)
	    System.err.println();
	return true;
    }


    // checks a recently modified location to see if there is a gene start site within
    // 3 bases up/down, and returns the location if it exists within the specified
    // chromosome arm, or -1 otherwise
    private static int checkLocalLoc(int loc, byte[][] chrom, int which){
	int len = chrom[which].length*2;
	int front = Math.max(0, loc-3);
	int end = Math.min(front + 7, len-72);
	if(end - front < 4) // Can't have a start site with less than 4 bases!
	    return -1;

	/* Some reasoning for the following: The region of interest may cover 1, 2, or 3 bytes 
	 * depending on size and offset. What I am doing is assuming that 3 bytes will be used, 
	 * and then using the masks to mask part of or entire bytes which will not be used.
	 */
	int rightlen = (2 * (4 - end%4));
	//              Set required number of bits to 1  Shift right to mask end bytes
	int wholemask = (int)Math.pow(2, 2*(end-front)-1) << rightlen;
	//           byte left of loc if it exists              byte which has loc       byte right of loc
	int region = ((loc/4 > 0)? chrom[which][loc/4-1]<<16:0)+(chrom[which][loc/4]<<8)+(chrom[which][loc/4+1]);
	// mask the region and then shift it right so we're only left with bits that matter
	int maskedRegion = (region & wholemask) >> rightlen;
	int found = -1;
	boolean one = false;
	for(int i = end-front; i >= 3;){
	    int zeroCount = Integer.numberOfTrailingZeros(maskedRegion);
	    // if there are no trailing zeroes, but the 2nd bit is 0, then the rightmost base maskedRegion
	    // represents is 1, and we want to see if enough zeroes preceed it to correspond to a gene start site
	    one = zeroCount == 0 && (maskedRegion & 2) == 0;
	    maskedRegion >>= 2;
	    zeroCount = Integer.numberOfTrailingZeros(maskedRegion);
	    if(one && zeroCount >= 6){
		found = i - 3;
		break;
	    }
	    maskedRegion >>= zeroCount*2;
	    i -= 2 + 2*(zeroCount/2+1);
	}

	if(found >= 0){
	    return found+front;
	}
	else
	    return -1;	
    }

    // checks a recently modified location to see if there is a gene start site within
    // 3 bases up/down now, and returns the location if it exists within the specified
    // chromosome arm, or -1 otherwise
    private static int checkLocalLoc(int loc, BitSet[] chromB, int len, int which){
	int front = Math.max(0, loc-3);
	int end = Math.min(front + 7, len-72);
	if(end - front < 4)
	    return -1;
	BitSet region = chromB[which].get(front*2, end*2);
	int foundB = -1;
	for(int i = 0; i < (end-front)*2; i++){
	    int test = region.nextSetBit(i);
	    if((test-1)%2 == 0 && test - i >= 7){
		foundB = test/2-3;
		break;
	    }
	    i = (test>=0)? test : i;
	}
	if(foundB >= 0){
	    return foundB+front;
	}
	else
	    return -1;	
    }
    
    public void recInit(Genome g, int wc, Random rand, double sceRate, double ptRate){	
	byte[][] c1 = g.gen[wc].chrom;
	byte[][] c2 = g.gen[wc+1].chrom;
	int[] pl1 = g.gen[wc].lens;
	int[] pl2 = g.gen[wc+1].lens;

	SortedSet<Integer> gl1 = g.gen[wc].geneLocView;
	SortedSet<Integer> gl2 = g.gen[wc+1].geneLocView;
	int numBreaks = -1;
	int sceMin = 1;
	float shorter1 = Math.max(Math.min(pl1[0]-sceMin, pl2[0]-sceMin), 0);
	float shorter2 = Math.max(Math.min(pl1[1]-sceMin, pl2[1]-sceMin), 0);
	if(SimStart.recMeth)
	    numBreaks = poisSamp(sceRate, rand);
	else{
	    numBreaks = poisSamp(sceRate*(shorter1+shorter2), rand);
	}
	// co for chosen one
	int co = rand.nextInt(2);

	lens[0] = 0;
	lens[1] = 0;

	if(numBreaks < 1){
	    if(co == 0){
		chrom[0] = new byte[c1[0].length];
		System.arraycopy(c1[0], 0, chrom[0], 0, c1[0].length);
		chrom[1] = new byte[c1[1].length];
		System.arraycopy(c1[1], 0, chrom[1], 0, c1[1].length);
		lens[0] = pl1[0];
		lens[1] = pl1[1];
		reinit(sceRate, new TreeSet<Integer>(gl1));
	    }
	    else{
		chrom[0] = new byte[c2[0].length];
		System.arraycopy(c2[0], 0, chrom[0], 0, c2[0].length);
		chrom[1] = new byte[c2[1].length];
		System.arraycopy(c2[1], 0, chrom[1], 0, c2[1].length);
		lens[0] = pl2[0];
		lens[1] = pl2[1];
		reinit(sceRate, new TreeSet<Integer>(gl2));
	    }
	    return;
	}
	int[] subBreaks = new int[2];
	// evenly distributing the number of breaks across arms
	subBreaks[0] = Math.round(numBreaks * (shorter1/(shorter1+shorter2)));
	subBreaks[1] = Math.round(numBreaks * (shorter2/(shorter1+shorter2)));
	int j;
	int maxFirstArm = Math.max(pl1[0], pl2[0]);
	int maxSecondArm = Math.max(pl1[1], pl2[1]);

	TreeSet<Integer> ngl = new TreeSet<Integer>();
	boolean match;
	boolean flip = false;
	// making breaks and attempting to cross
	for(int w = 0; w < 2; w++){
	    byte[][] lc;
	    byte[][] sc;
	    SortedSet<Integer> sgl;
	    SortedSet<Integer> lgl;
	    int lrgLen = -1;
	    int smlLen = -1;

	    // ArrayLists are faster to make than TreeSets, and since we need to modify
	    // the values of gene locations in the first arm of each chromosome by
	    // subtracting the length of that arm and we don't actually know the length
	    // until it has already been built and new gene locations filled out, it is
	    // faster to just allocate two container objects and use the first to fill in the 2nd
	    // with the modified values than to add/remove repeatedly on a TreeSet
	    ArrayList<Integer> tgl = new ArrayList<Integer>();

	    if(pl1[w] > pl2[w]){
		lc = c1;
		sc = c2;
		lrgLen = pl1[w];
		smlLen = pl2[w];
		lgl = gl1;
		sgl = gl2;
	    }
	    else{
		lc = c2;
		sc = c1;
		lrgLen = pl2[w];
		smlLen = pl1[w];
		lgl = gl2;
		sgl = gl1;
	    }
	    int lOff = (w == 0)? -lrgLen : 0;
	    int sOff = (w == 0)? -smlLen : 0;

	    
	    ArrayList<int[]> matchIndex = new ArrayList<>();
	    int loopCond = Math.min(smlLen, matchIndex.size() * 100);
	    int zeroCount = 0;
	    TreeSet<Integer> lookLocs = new TreeSet<>();
	    int maxShift = 100;
	    int recRange = Math.max(1, smlLen-maxShift+1-sceMin);
	    while(lookLocs.size() < Math.min(subBreaks[w], recRange)){
		int nextCand = rand.nextInt(recRange/4);
		lookLocs.add(nextCand*4);
	    }
	    int prevL = -1;
	    int prevS = -1;

	    for(int lookLoc : lookLocs){
		for(int lookOff = 0; lookOff < maxShift; lookOff += 4){
		    if(lookOff < smlLen - sceMin &&
		       lookLoc + lookOff > prevL &&
		       Integer.bitCount((sc[w][lookLoc/4]&0xFF)^(lc[w][(lookLoc+lookOff)/4]&0xFF)) <= 2*sceMin){
			matchIndex.add(new int[]{lookLoc, lookOff});
			prevL = lookLoc + lookOff;
			break;
		    }
		    if(lookLoc - lookOff > prevL &&
		       Integer.bitCount((sc[w][lookLoc/4]&0xFF)^(lc[w][(lookLoc-lookOff)/4]&0xFF)) <= 2*sceMin){
			matchIndex.add(new int[]{lookLoc, -lookOff});
			prevL = lookLoc - lookOff;
			break;
		    }
		}
	    }

	    if(matchIndex.size() < lookLocs.size()){
		
		reinit(sceRate,ngl);
		return;
	    }

	    int smlStart = 0;
	    int lrgStart = 0;
	    ArrayList<Integer> check = new ArrayList<Integer>();
	    int len = 0;
	    boolean pflip = flip;
	    for(int[] b : matchIndex){
		if(flip){
		    if(co == 0)
			len += b[0] - smlStart;
		    else
			len += b[0]+b[1] - lrgStart;
		    smlStart = b[0];
		    lrgStart = b[0]+b[1];
		    flip = false;
		}
		else{
		    if(co == 0)
			len += b[0]+b[1] - lrgStart;
		    else
			len += b[0] - smlStart;
		    smlStart = b[0];
		    lrgStart = b[0]+b[1];
		    flip = true;
		}

	    }
	    if(flip){
		if(co == 0)
		    len += smlLen - smlStart;
		else
		    len += lrgLen - lrgStart;		
	    }
	    else{
		if(co == 0)
		    len += lrgLen - lrgStart;
		else
		    len += smlLen - smlStart;
	    }
	    int armLen = len;
	    if(armLen % 4 != 0){
		System.err.println("mmm");
		SimStart.shutDownEverything();
		System.exit(1);
	    }
	    flip = pflip;
	    smlStart = 0;
	    lrgStart = 0;
	    len = 0;
	    chrom[w] = new byte[armLen/4];
	    for(int[] b : matchIndex){
		if(SimStart.verbose)
		    System.out.println("b\t" + (b[0] +lOff) + "\t" + b[1]);
		if(flip){
		    // adding/subtracting by 3 so that we won't add any gene start
		    // sites which span the recombination breakpoint. We'll be searching
		    // these sites later to see if any gene start sites span these points
		    if(co == 0){
			if(smlStart+1 <= b[0]-2)
			    for(int gl : sgl.subSet(smlStart+sOff+1, b[0]+sOff-2)){
				int subLoc = gl - (smlStart + sOff);
				tgl.add(len/2 + subLoc);
			    }
			crossOver(sc[w], smlStart/4, chrom[w], len/8, (b[0]-smlStart)/4);
			len += 2*(b[0] - smlStart);
			check.add(len/2);
		    }
		    else{
			if(lrgStart+1 <= b[0]+b[1]-2)
			    for(int gl : lgl.subSet(lrgStart+lOff+1, b[0]+b[1]+lOff-2)){
				int subLoc = gl - (lrgStart + lOff);
				tgl.add(len/2 + subLoc);
			    }
			crossOver(lc[w], lrgStart/4, chrom[w], len/8, ((b[0]+b[1])-lrgStart)/4);
			len += 2*(b[0]+b[1] - lrgStart);
			check.add(len/2);
		    }
		    smlStart = b[0];
		    lrgStart = b[0]+b[1];
		    flip = false;
		}
		else{
		    if(co == 0){
			if(lrgStart+1 <= b[0]+b[1]-2)
			    for(int gl : lgl.subSet(lrgStart+lOff+1, b[0]+b[1]+lOff-2)){
				int subLoc = gl - (lrgStart + lOff);
				tgl.add(len/2 + subLoc);
			    }
			crossOver(lc[w], lrgStart/4, chrom[w], len/8, ((b[0]+b[1])-lrgStart)/4);
			len +=  2*(b[0]+b[1] - lrgStart);
			check.add(len/2);
		    }
		    else{
			if(smlStart+1 <= b[0]-2)
			    for(int gl : sgl.subSet(smlStart+sOff+1, b[0]+sOff-2)){
				int subLoc = gl - (smlStart + sOff);
				tgl.add(len/2 + subLoc);
			    }
			crossOver(sc[w], smlStart/4, chrom[w], len/8, (b[0]-smlStart)/4);
			len += 2*(b[0] - smlStart);
			check.add(len/2);
		    }
		    smlStart = b[0];
		    lrgStart = b[0]+b[1];
		    flip = true;
		}
	    }
	    if(flip){
		if(co == 0){
		    if(smlStart+1 <= smlLen-2)
			for(int gl : sgl.subSet(smlStart+sOff+1, smlLen+sOff-2)){
			    int subLoc = gl - (smlStart + sOff);
			    tgl.add(len/2 + subLoc);
			}
		    crossOver(sc[w], smlStart/4, chrom[w], len/8, (smlLen-smlStart)/4);
		    len += 2*(smlLen - smlStart);
		}
		else{
		    if(lrgStart+1 <= lrgLen-2)
			for(int gl : lgl.subSet(lrgStart+lOff+1, lrgLen+lOff-2)){
			    int subLoc = gl - (lrgStart + lOff);
			    tgl.add(len/2 + subLoc);
			}
		    crossOver(lc[w], lrgStart/4, chrom[w], len/8, (lrgLen-lrgStart)/4);
		    len += 2*(lrgLen - lrgStart);
		}
	    }
	    else{
		if(co == 0){
		    if(lrgStart+1 <= lrgLen-2)
			for(int gl : lgl.subSet(lrgStart+lOff+1, lrgLen+lOff-2)){
			    int subLoc = gl - (lrgStart + lOff);
			    tgl.add(len/2 + subLoc);

			}
		    crossOver(lc[w], lrgStart/4, chrom[w], len/8, (lrgLen-lrgStart)/4);
		    len += 2*(lrgLen - lrgStart);
		}
		else{
		    if(smlStart+3 <= smlLen-2)
			for(int gl : sgl.subSet(smlStart+sOff+1, smlLen+sOff-2)){
			    int subLoc = gl - (smlStart + sOff);
			    tgl.add(len/2 + subLoc);

			}
		    crossOver(sc[w], smlStart/4, chrom[w], len/8, (smlLen-smlStart)/4);
		    len += 2*(smlLen - smlStart);
		}
	    }
	    lens[w] = len/2;
	    if(lens[w] != chrom[w].length*4){
		System.err.println("this is bad");
		System.err.println(lens[w]);
		System.err.println(chrom[w].length*4);
		SimStart.shutDownEverything();
		System.exit(1);
	    }
	    int mod = (w == 0)? lens[0] : 0;
	    for(int gl : tgl){
		if(gl > lens[w] - 72)
		    continue;
		ngl.add(gl - mod);
	    }
	    
	    for(int i = 0; i < check.size(); i++){
		int gl = checkLocalLoc(check.get(i), chrom,  w);
		if(gl >= 0){
		    ngl.add(gl - mod);
		}
	    }
	}

	int num = poisSamp(ptRate * (lens[0] + lens[1]), rand);
	for(int i = 0; i < num; i++){
	    pointMutate(rand, chrom, ngl);
	}
	reinit(sceRate, ngl);
    }

    private static void crossOver(byte[] other, int ostart, byte[] chrom, int cstart, int len){
	try{
	    System.arraycopy(other, ostart, chrom, cstart, len);
	}
	catch(Exception e){
	    System.err.println(cstart);
	    System.err.println(ostart);
	    System.err.println(len);
	    System.err.println(chrom.length);
	    System.err.println(other.length);
	    System.err.println(cstart + len);
	    e.printStackTrace();
	    SimStart.shutDownEverything();	    
	    System.exit(1);
	}
    }
    
    // given mean u and one time interval,
    // samples a point from the corresponding
    // poisson distribution. (Knuth's implementation)
    private static int poisSamp(double u, Random rand){
	double eu = Math.pow(Math.E, -u);
	double p = 1;
	int result = 0;
	do{
	    result++;
	    p *= rand.nextDouble();
	}while(p > eu);
	
	return result-1;
    }

    // Finds the locations in a chromosome that encode a gene start site
    //
    // Should only need to be run when an entirely new genome is created.
    // Reproduction uses parental gene locations to create offspring gene locations
    // by just checking locations around where mutation/recombination happen.
    public TreeSet<Integer> findGenes(){
	//TreeSet<Integer> bm = new TreeSet<Integer>();
	TreeSet<Integer> gl = new TreeSet<Integer>();
	for(int j = 0; j < 2; j++){
	    int p = -1;
	    int off = (j == 0)? -lens[0] : 0;
	    int zeroCount = 0;	    
	    for(int i = 0; i < chrom[j].length; i++){
		// exactly this byte yay!
		if(chrom[j][i] == 1){
		    gl.add(i*4+off);
		    zeroCount = 0;
		    String t = "000" + Integer.toUnsignedString(chrom[j][i], 4);
		    continue;
		}

		// not eactly this one :( if the end of the previous one could start a
		// gene location let's see if the start of this one can
		if(zeroCount > 0){
		    // how many zero bits do we have leading this byte (-24 since this is an integer
		    // method)
		    int leadingZeros = Integer.numberOfLeadingZeros(chrom[j][i])-24;
		    // if the total number of zeros is 3 or more and they end in a 1,
		    // we found a start site!
		    if(leadingZeros/2 + zeroCount >= 3 && leadingZeros%2 == 1){
			int nl = leadingZeros/2 + 1 + (i-1)*4 + off;
			gl.add(nl);
			String t1 = "000" + Integer.toUnsignedString(chrom[j][i-1], 4);
			String t2 = "000" + Integer.toUnsignedString(chrom[j][i], 4);
		    }
		}		
		zeroCount = Integer.numberOfTrailingZeros(chrom[j][i])/2;
	    }
	}
	return gl;
    }


    byte[] getBGene(int loc){
	byte[] result = new byte[18];
	int w = -1;
	if(loc < 0){
	    loc = loc + lens[0];
	    w = 0;
	}
	else{
	    w = 1;
	}
	int shift = 2 * (loc%4);
	int mask = ((1<<shift)-1) << (8-shift);
	for(int i = 0; i < 18; i++){
	    result[i] = (byte)(chrom[w][loc/4+i] << shift);
	    if(shift > 0){
		result[i] ^= (byte)((chrom[w][loc/4+i+1]&mask) >>> (8-shift));
	    }
	}
	return result;
    }

    /*
    boolean sanityTest(){
	TreeSet<Integer> fg = findGenes();
	if(!geneLocs.equals(fg)){
	    String errOut = "";
	    TreeSet<Integer> temp = new TreeSet<Integer>(geneLocs);
	    temp.removeAll(fg);
	    for(int k : temp){
		errOut += k + "\t0\n";
		if(k < 0)
		    errOut += checkLocalLoc(k+lens[0], chromB, lens[0], 0) + "\n";
		else
		    errOut += checkLocalLoc(k, chromB, lens[1], 1) + "\n";
	    }
	    temp = new TreeSet<Integer>(fg);
	    temp.removeAll(geneLocs);
	    for(int k : temp){		
		errOut += k + "\t1\n";
		if(k < 0)
		    errOut += checkLocalLoc(k+lens[0], chromB, lens[0], 0) + "\n";
		else
		    errOut += checkLocalLoc(k, chromB, lens[1], 1) + "\n";
	    }
	    errOut += "---\n";
	    for(int k : geneLocs){
		errOut += bsString(getGene(k),144) + "\t" + k + "\n";
	    }
	    errOut += "---\n";
	    for(int k : fg){
		errOut += bsString(getGene(k),144) + "\t" + k + "\n";
	    }
	    errOut += "---\n";
	    errOut += lens[0] + "\t" + lens[1] + "\n";
	    for(StackTraceElement elem : Thread.currentThread().getStackTrace()){
		errOut += elem + "\n";
	    }
	    System.err.println(errOut);
	    return false;
	}
	return true;
    }
    */
}

