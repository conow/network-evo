import java.util.*;
import java.util.concurrent.*;
import java.util.zip.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.StandardCopyOption.*;
import java.math.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;


public class SimStart{
    private static FileOutputStream fos;
    private static GZIPOutputStream gos;
    private static ObjectOutputStream oos;

    static ExecutorService pool;
    static CompletionService<LiveCell> compS;
    static int poolRemaining;
    static ExecutorService output;

    static int i = 0;
    static boolean done;
    private static Scanner kb = new Scanner(System.in);
    static final boolean verbose = false;
    static final Calendar now = new GregorianCalendar();
    static String baseDumpOut = "/home/cmb-panasas2/conow/simulation_out/" + 
	now.get(Calendar.MONTH) + "_" + now.get(Calendar.DATE) + "_" + now.get(Calendar.YEAR) + "/dump/";
    static String baseStatOut = "/home/cmb-panasas2/conow/simulation_out/" + 
	now.get(Calendar.MONTH) + "_" + now.get(Calendar.DATE) + "_" + now.get(Calendar.YEAR) + "/stats/";
    static String baseLineOut = "/home/cmb-panasas2/conow/simulation_out/" + 
	now.get(Calendar.MONTH) + "_" + now.get(Calendar.DATE) + "_" + now.get(Calendar.YEAR) + "/lines/";
    static String baseNetOut = "/home/cmb-panasas2/conow/simulation_out/" + 
	now.get(Calendar.MONTH) + "_" + now.get(Calendar.DATE) + "_" + now.get(Calendar.YEAR) + "/lines/";
    static boolean baseDump = false;
    static boolean statDump = false;
    static boolean lineDump = false;
    static String sFloc;
    static String faLoc;
    static boolean dumpOut = false;
    static boolean statOut = false;
    static boolean lineOut = false;
    static boolean sex = true;
    static boolean recomb = true;
    static boolean setSeed = false;
    static boolean settingsFile = false;
    static final Random r = new Random();
    static long initSeed = r.nextLong();
    static {
	r.setSeed(initSeed);
    }
    static double ptRate = 0.0001;
    static double rcRate = 2.0;
    static int maxIter = 50001;
    static int numMols = 53;
    static double molCon = 50.0;
    static int bh = 30;
    static int bw = 30;
    static int randEnv = -1;
    static int burnIn = 5000;
    static double propKill = 1/16.;
    static Path genomeFile = Paths.get("mod.gen");
    static boolean recMeth = true;
    static int numCores;
    
    // this controls whether we're selecting for individuals with high or low fitness levels
    static int selectionDirection = 1;//-1;
    static double killSelect = 10;
    static double repSelect = 10;
    static int fitFunc = 0;
    static double targetMax = 5.0;
    static double targetMin = 1.0;
    static int targetLength = 100;
    static boolean restartFromSnapshot = false;
    static Path snapPath;
    static int matAge = 7;

    static int setReactNum = -1;
    
    // this is a redundant variable. I'm sorry
    static int iternum = 0;
    static int iterout = 5000;

    static Path targetFile;
    static Path envFile = Paths.get("envInit.rich");
    static boolean useTF = false;
    static boolean netAnalysis = false;
    static boolean conSensitivity = false;
    static boolean geneSensitivity = false;
    static boolean crossPops = false;
    static Path pop1;
    static Path pop2;
    static double[] p;
    static BufferedWriter f = new BufferedWriter(new OutputStreamWriter(System.out));;

    static int fn = -1;
    static boolean useReps = false;
    
    // flags so that we can override settings file flags with flags from the command line
    //
    //
    // ...flags
    static boolean nsSet = false;
    static boolean doSet = false;
    static boolean soSet = false;
    static boolean nrSet = false;
    static boolean ssSet = false;
    static boolean sfSet = false;
    static boolean loSet = false;
    static boolean nmSet = false;
    static boolean niSet = false;
    static boolean mcSet = false;
    static boolean bsSet = false;
    static boolean reSet = false;
    static boolean bpSet = false;
    static boolean pkSet = false;
    static boolean gfSet = false;
    static boolean mrSet = false;
    static boolean rmSet = false;
    static boolean rrSet = false;
    static boolean ntSet = false;
    static boolean ffSet = false;
    static boolean tmSet = false;
    static boolean tlSet = false;
    static boolean tiSet = false;
    static boolean rnSet = false;
    static boolean tfSet = false;
    static boolean rsSet = false;
    static boolean naSet = false;
    static boolean cpSet = false;
    static boolean csSet = false;
    static boolean gsSet = false;
    static boolean faSet = false;
    static boolean ksSet = false;
    static boolean slSet = false;
    static boolean maSet = false;
    static boolean enSet = false;
    static boolean fnSet = false;

    public static void main(String [] args) throws IOException, DeathCountException{
	try{
	    numCores = Integer.parseInt(System.getenv("NUMCORES")) - 1;
	}
	catch(NumberFormatException ex){
	    System.err.println("Warning: could not detect number of cores -- numCores set to 4");
	    numCores = 4;
	}
	parseArgs(args);

	System.out.println(initSeed);

	if(settingsFile == true){
	    readSettings();
	}

	Environment e;
	p = new double[608];
	BufferedReader in = new BufferedReader(new FileReader(envFile.toString()));
	String s = "";
	for(int i = 0; i < 608 && (s = in.readLine()) !=  null; i++){
	    p[i] = Double.parseDouble(s);
	}

	pool = Executors.newFixedThreadPool(numCores);
	compS = new ExecutorCompletionService<LiveCell>(pool);
	output = Executors.newCachedThreadPool();

	e = initE();
	
	if(dumpOut){
	    baseDumpOut += "/" + (sex? "s" + (recomb? "_r" : "_nr") : "ns") + "/";
	    baseDumpOut += makeDirs(baseDumpOut, e, fn) + "/";
	}
	if(statOut){
	    baseStatOut += "/" + (sex? "s" + (recomb? "_r" : "_nr") : "ns") + "/";
	    baseStatOut += makeDirs(baseStatOut, e, fn) + "/";
	}
	if(lineOut){
	    baseLineOut += "/" + (sex? "s" + (recomb? "_r" : "_nr") : "ns") + "/";
	    baseLineOut += makeDirs(baseLineOut, e, fn) + "/";
	    baseDumpOut = baseLineOut;
	}
	if(netAnalysis){
	    baseNetOut += "/nets/";
	    baseNetOut += makeDirs(baseNetOut, e, fn) + "/";
	}
	if(setSeed){
	    r.setSeed(initSeed);
	}	
	
	Long start = System.nanoTime();
	int i = 0;
	Genome fittest = null;
	done = false;
	//LinkedBlockingQueue<Future> runningTasks = new LinkedBlockingQueue<Future>(e.width*e.height);

	/* there needs to be a more flexible way to set up experiments. Not sure what at the moment, but we'll
	 * figure something out.
	 */
	if(conSensitivity){
	    // want to have a "normal" run to compare to
	    e.runWithP(0, 1);
	    for(i = 0; i < numMols; i++){
		e.runWithP(i, 0.5);
		e.runWithP(i, 0);
	    }
	}

	if(geneSensitivity){
	    e.run(maxIter);
	    // all about that
	    String basebase = baseNetOut;
	    int count = 0;
	    for(LiveCell lc : e.liveCells){
		Environment allKnockout = new Environment(lc.internalReactions.size() + 1, 1);
		allKnockout.cellListInit(p, numMols, randEnv, burnIn);		
		ArrayList<LiveCell> knockoutStrains = new ArrayList<>();
		LiveCell wild = new LiveCell(new int[]{0, knockoutStrains.size()}, allKnockout,  lc);
		knockoutStrains.add(wild);
		for(Reaction r : lc.internalReactions.values()){
		    // no sense in calculating stuff for unreachable genes
		    if(r.sat){
			LiveCell temp;
			for(int k = 0; k < 1; k++){
			    temp = new LiveCell(new int[]{0, knockoutStrains.size()}, allKnockout,  lc);
			    temp.spike(r.type, r.reactionNum, r.throughput*(1-k/10.));
			    knockoutStrains.add(temp);
			}
		    }
		}
		if(knockoutStrains.size() == 0)
		    continue;
		baseNetOut = basebase + "/" + lc.loc[0]+"_"+lc.loc[1] + "/";
		count++;
		
		File outDir = new File(baseNetOut + "/");;
		outDir.mkdirs();
		f = new BufferedWriter(new FileWriter(baseNetOut+"stdOut"));
		allKnockout.addInds(knockoutStrains);
		allKnockout.run(maxIter);
	    }
	}

	else{
	    do{
		// this is ugly but it just needs to work for now
		e = initE();
		e.run(maxIter);
	    }while(iternum < maxIter -1);
	}
	f.flush();
	f.close();
	output.shutdown();
	pool.shutdown();
	System.out.println((System.nanoTime() - start)/1000000000.);
    }

    private static Environment initE() throws IOException, DeathCountException{
	Environment e;
	if(restartFromSnapshot){
	    e = Environment.fromDump(snapPath, p, randEnv, numMols, burnIn, propKill, false);
	}
	else if(crossPops){
	    Environment e1 = Environment.fromDump(pop1, p, randEnv, numMols, burnIn, 0, true);
	    Environment e2 = Environment.fromDump(pop2, p, randEnv, numMols, burnIn, 0, true);
	    e = Environment.crossPops(e1, e2);
	}
	else{
	    e = new Environment(bw, bh);
	    Genome test = new Genome(genomeFile, new Random(r.nextLong()), ptRate, rcRate);
	    e.standardInit(test, p, ptRate, rcRate, randEnv, numMols, burnIn, propKill);
	}
	return e;
    }

    // need to set this up so that it doesn't override command line flags
    private static void readSettings() throws IOException{
	JsonNode sf = Environment.m.readTree(new FileInputStream(sFloc));
	if(!mrSet){ptRate = sf.get("ptRate").asDouble();}
	if(!rrSet){rcRate = sf.get("rcRate").asDouble();}
	// currently no command line flag for indels. Should probably add that...
	if(!nmSet){numMols = sf.get("numMols").asInt();}
	if(!mcSet){molCon = sf.get("molCon").asDouble();}
	if(!niSet){maxIter = sf.get("maxIter").asInt();}
	if(!bsSet){bh = sf.get("bh").asInt();}
	if(!bsSet){bw = sf.get("bw").asInt();}
	if(!reSet){randEnv = sf.get("randEnv").asInt();}
	if(!bpSet){burnIn = sf.get("burnIn").asInt();}
	if(!pkSet){propKill = sf.get("propKill").asDouble();}
	if(!rmSet){recMeth = sf.get("recMeth").asBoolean();}
	if(!ffSet){
	    selectionDirection = sf.get("selectionDirection").asInt();
	    fitFunc = sf.get("fitFunc").asInt();
	}
	if(!tfSet){
	    useTF = sf.get("useTF").asBoolean();
	    if(useTF)
		targetFile = Paths.get(sf.get("targetFile").asText());	    
	}
	if(fitFunc != 1 && !useTF){
	    if(!tmSet){targetMax = sf.get("targetMax").asDouble();}
	    if(!tiSet){targetMin = sf.get("targetMin").asDouble();}
	    if(!tlSet){targetLength = sf.get("targetLength").asInt();}
	}
	if(!rsSet){	    
	    restartFromSnapshot = sf.get("restartFromSnapshot").asBoolean();
	    if(restartFromSnapshot)
		snapPath = Paths.get(sf.get("snapPath").asText());
	    else
		if(!gfSet){genomeFile = Paths.get(sf.get("genomeFile").asText());}
	}

	if(!rnSet){setReactNum = sf.get("setReactNum").asInt();}
	if(!cpSet){
	    crossPops = sf.get("crossPops").asBoolean();
	    if(crossPops){
		pop1 = Paths.get(sf.get("pop1").asText());
		pop2 = Paths.get(sf.get("pop2").asText());
	    }
	}
	if(!nsSet){sex = sf.get("sex").asBoolean();}
	if(!nrSet){recomb = sf.get("recomb").asBoolean();}
	// fix this later. anything in here means that a previous version
	// of the simulation did not have these items
	try{
	    if(!maSet){matAge = sf.get("matAge)").asInt();}
	    if(!ksSet){killSelect = sf.get("killSelect").asDouble();}
	    if(!slSet){repSelect = sf.get("repSelect").asDouble();}
	    if(!enSet){envFile = Paths.get(sf.get("envFile").asText());}
	}
	catch(NullPointerException npe){
	    System.err.println(npe.getMessage());
	    System.err.println("WARNING: some options are missing from settings file. Using default values");
	}
	// this is kind of jank, but whatever
	if(faSet){
	    snapPath = Paths.get(faLoc + "/post/" + (maxIter-1) + ".out.gz");
	    if(!Files.exists(snapPath))
		snapPath = Paths.get(faLoc + "/pre/" + (maxIter-1) + ".out.gz");
	    maxIter = 50;
	}
    }
    
    private static int makeDirs(String out, Environment e, int n) throws IOException{
	File outDirBase = new File(out);
	while(!outDirBase.exists()){
	    boolean made = outDirBase.mkdirs();
	}
	int numInFolder = outDirBase.listFiles().length;
	if(n > -1)
	    numInFolder = n;
	out += numInFolder + "/";
	File outDir = new File(out);
	if(outDir.exists()){
	    System.err.println("YA DONE GOOFED!");
	    System.exit(1);
	}
	outDir.mkdirs();
	File outDirPre = new File(out+"/pre/");
	File outDirPost = new File(out+"/post/");
	outDirPre.mkdirs();
	outDirPost.mkdirs();

	FileWriter fw = new FileWriter(out + "settings");
	TreeMap<String, Object> settingsFile = new TreeMap<>();
	settingsFile.put("ptRate", ptRate);
	settingsFile.put("rcRate", rcRate);
	settingsFile.put("initSeed", initSeed);
	settingsFile.put("numMols", numMols);
	settingsFile.put("molCon", molCon);
	settingsFile.put("bh", bh);
	settingsFile.put("bw", bw);
	settingsFile.put("randEnv", randEnv);
	settingsFile.put("burnIn", burnIn);
	settingsFile.put("propKill", propKill);
	settingsFile.put("maxIter", maxIter);
	if(!restartFromSnapshot)
	    settingsFile.put("genomeFile", genomeFile.toString());
	settingsFile.put("recMeth", recMeth);
	settingsFile.put("numCores", numCores);
	settingsFile.put("selectionDirection", selectionDirection);
	settingsFile.put("fitFunc", fitFunc);
	if(fitFunc != 1 && !useTF){
	    settingsFile.put("targetMax", targetMax);
	    settingsFile.put("targetMin", targetMin);
	    settingsFile.put("targetLength", targetLength);
	}
	settingsFile.put("restartFromSnapshot", restartFromSnapshot);
	if(restartFromSnapshot)
	    settingsFile.put("snapPath", snapPath.toString());
	settingsFile.put("setReactNum", setReactNum);
	settingsFile.put("useTF", useTF);
	if(useTF)
	    settingsFile.put("targetFile", targetFile.toString());
	settingsFile.put("crossPops", crossPops);
	if(crossPops){
	    settingsFile.put("pop1", pop1.toString());
	    settingsFile.put("pop2", pop2.toString());
	}
	settingsFile.put("sex", sex);
	settingsFile.put("recomb", recomb);
	settingsFile.put("matAge", matAge);
	settingsFile.put("killSelect", killSelect);
	settingsFile.put("repSelect", repSelect);
	settingsFile.put("envFile", envFile);
	// using the json mapper defined in environment to write the file...
	// this seems kind of incorrect but whatever
	fw.write(Environment.m.writeValueAsString(settingsFile));
	if(fitFunc != 1){
	    fw.flush();
	    fw.close();       
	    fw = new FileWriter(out + "target");
	    for(int i = 0; i < 608; i++){
		fw.write(e.target[0][i] + "\t" + e.target[1][i] + "\n");
	    }
	}	
	fw.flush();
	fw.close();
	Files.copy(envFile, Paths.get(out + "envInit"));
	if(restartFromSnapshot)
	    Files.copy(snapPath, Paths.get(out + "basis.out.gz"));
	if(crossPops){
	    Files.copy(pop1, Paths.get(out + "pop1.out.gz"));
	    Files.copy(pop2, Paths.get(out + "pop2.out.gz"));
	}
	f.flush();
	f.close();
	f = new BufferedWriter(new FileWriter(out+"stdOut"));
	return numInFolder;
    }
    
    static void shutDownEverything(){
	System.out.println("Shutting down thread pools...");
	output.shutdown();
	pool.shutdown();
    }

    private static void parseArgs(String[] args){
	for(int i = 0; i < args.length; i++){
	    switch(args[i]){
	    case "-ns": // ns for no sexual reproduction. Just a flag
		nsSet = true;
		sex = false;
		recomb = false;
		break;
	    case "-do": // do for dump out. Additionally requires a path to write files. Outputs "full" information every 5000 iterations
		doSet = true;
		dumpOut = true;
		if(i + 1 < args.length){
		    i++;
		    baseDumpOut = args[i];
		}
		break;
	    case "-so": // so for stat out. Additionally requires a path to write files. Outputs summary stats every 5000 iterations
		soSet = true;
		statOut = true;
		if(i + 1 < args.length){
		    i++;
		    baseStatOut = args[i];
		}
		break;
	    case "-nr": // nr for no recombination. Just a flag
		nrSet = true;
		recomb = false;
		break;
	    case "-ss": // ss for set seed. Allows (in theory) replicate runs to gather more data. This hasn't been tested in a while though, and with parallel processing who knows
		ssSet = true;
		setSeed = true;
		if(i + 1 < args.length){
		    i++;
		    initSeed = Long.parseLong(args[i]);
		}
		break;
	    case "-sf": // sf for settings file. Requires a path, and then reads mutational rates from it
		sfSet = true;
		settingsFile = true;
		if(i + 1 < args.length){
		    i++;
		    sFloc = args[i];
		}
		else{
		    System.err.println("Need to specify file path for settings file!");
		    System.exit(1);
		}
		break;
	    case "-lo": // lo for line out. Additionally requires a path to write files. Essentially dump out but every iteration. Also outputs lineage information
		loSet = true;
		lineOut = true;
		if(i + 1 < args.length){
		    i++;
		    baseLineOut = args[i];
		}
		break;
	    case "-nm": // nm for number of molecules. Requires an integer. Then molecules with index in the range of [0, n) will be produced.
		nmSet = true;
		if(i + 1 < args.length){
		    i++;
		    numMols = Integer.parseInt(args[i]);
		}
		break;
	    case "-ni": // ni for number of iterations. Requires an integer.
		niSet = true;
		if(i + 1 < args.length){
		    i++;
		    maxIter = Integer.parseInt(args[i]);
		}
		break;
	    case "-mc": // mc for molecular concentration. Requires a float. Specifies how much of each molecule the environment produces per iteraion, or the max in the case that the environment randomizes
		mcSet = true;
		if(i + 1 < args.length){
		    i++;
		    molCon = Double.parseDouble(args[i]);
		}
		break;
	    case "-bs": // bs for boardsize. Requires input of the form nxm where n and m are integers.
		bsSet = true;
		if(i + 1 < args.length){
		    i++;
		    String a = args[i];
		    String[] wh = a.split("x");
		    bw = Integer.parseInt(wh[0]);
		    if(wh.length > 1)
			bh = Integer.parseInt(wh[1]);
		    else
			bh = 1;
		}
		break;
	    case "-re": // re for randomized environment. Takes an integer which specifies the period of randomization
		reSet = true;
		if(i + 1 < args.length){
		    i++;
		    randEnv = Integer.parseInt(args[i]);
		}
		break;
	    case "-bp": // bp for burn period for random environments. For some parameter sets, the initial population may require some time to adapt before it can survive randomization.
		bpSet = true;
		if(i + 1 < args.length){
		    i++;
		    burnIn = Integer.parseInt(args[i]);
		}
		break;
	    case "-pk": // pk for proportion to kill per iteration. Takes a float in the range [0, 1]
		pkSet = true;
		if(i + 1 < args.length){
		    i++;
		    propKill = Double.parseDouble(args[i]);
		}
		break;
	    case "-gf": // gf for genome file. Allows you to specify where it's reading the initial genome from
		gfSet = true;
		if(i + 1 < args.length){
		    i++;
		    genomeFile = Paths.get(args[i]);
		}
		break;
	    case "-mr": // mr for mutation rate per base. Takes as input a float
		mrSet = true;
		if(i + 1 < args.length){
		    i++;
		    ptRate = Double.parseDouble(args[i]);
		}
		break;
	    case "-rm": // rm for recombination method. Defaults to prob per chrom, using this flag makes it per base
		rmSet = true;
		recMeth = false;
		break;
	    case "-rr": // rr for recombination rate per chromosome by default, or per base if rm flag is used. Takes as input a flot
		rrSet = true;
		if(i + 1 < args.length){
		    i++;
		    rcRate = Double.parseDouble(args[i]);		    
		}
		break;
	    case "-nt": // nt for number of threads. Use this to specify number of threads to use for running the simulation
		ntSet = true;
		if(i +1 < args.length){
		    i++;
		    numCores = Integer.parseInt(args[i]);
		}
		break;
	    case "-ff": // ff for fitness function. Right now there 3
		ffSet = true;
		if(i + 1 < args.length){
		    i++;
		    fitFunc = Integer.parseInt(args[i]);
		}
		break;
	    case "-tm": // tm for target max
		tmSet = true;
		if(i + 1 < args.length){
		    i++;
		    targetMax = Double.parseDouble(args[i]);
		}
		break;
	    case "-tl": // tl for target length
		tlSet = true;
		if(i + 1 < args.length){
		    i++;
		    targetLength = Integer.parseInt(args[i]);
		}
		break;
	    case "-ti": // ti for target min (since tm is taken)
		tiSet = true;
		if(i + 1 < args.length){
		    i++;
		    targetMin = Double.parseDouble(args[i]);
		}
		break;
	    case "-rn": // rn for reaction number. There are 1.1 million reactions which ONLY have to do with shuffling bases in length 12 molecules. 300k do everything else
		rnSet = true;
		if(i + 1 < args.length){
		    i++;
		    setReactNum = Integer.parseInt(args[i]);
		}
		break;
	    case "-tf": // tf for target file
		tfSet = true;
		if(i + 1 < args.length){
		    i++;
		    useTF = true;
		    targetFile = Paths.get(args[i]);
		}
		break;
	    case "-rs": // rs for restart (from) snapshot
		rsSet = true;
		if(i + 1 < args.length){
		    i++;
		    restartFromSnapshot = true;
		    snapPath = Paths.get(args[i]);
		}
		break;
	    case "-na": // na for network analysis.
		naSet = true;
		if(i + 1 < args.length){
		    i++;
		    baseNetOut = args[i];
		}
		netAnalysis = true;
		propKill = 0.0;
		pkSet = true;
		break;
	    case "-cp": // cp for cross pops
		cpSet = true;
		if(i + 2 < args.length){
		    i++;
		    pop1 = Paths.get(args[i]);
		    i++;
		    pop2 = Paths.get(args[i]);
		    crossPops = true;
		}
		propKill = 0.0;
		break;
	    case "-cs": // cs for concentration sensitivity
		csSet = true;
		if(netAnalysis){
		    conSensitivity = true;
		    break;
		}
		else{
		    System.err.println("Invalid flag: -cs\tCan only measure concentration sensitivity when doing network analysis!");
		    System.exit(1);
		}
		break;
	    case "-gs": // gs for gene sensitivity
		gsSet = true;
		if(netAnalysis){
		    geneSensitivity = true;
		    break;		   
		}
		else{
		    System.err.println("Invalid flag: -gs\tCan only measure concentration sensitivity when doing network analysis!");
		    System.exit(1);
		}
		break;
	    case "-ks": // ks for kill selection coefficient
		ksSet = true;
		if(i + 1 < args.length){
		    i++;
		    killSelect = Double.parseDouble(args[i]);
		}
		break;
	    case "-sl": // sl for selection coefficient (for reproduction)
		slSet = true;
		if(i + 1 < args.length){
		    i++;
		    repSelect = Double.parseDouble(args[i]);
		}
		break;
	    case "-fa": // fa for final analysis. Basically I'm lazy and just want to pass a input and output paths without other
		        // flags
		faSet = true;
		gsSet = true;
		netAnalysis = true;
		naSet = true;
		geneSensitivity = true;
		sfSet = true;
		settingsFile = true;
		if(i + 1 < args.length){
		    i++;
		    faLoc = args[i];
		    sFloc = args[i] + "/settings";
		}
		else{
		    System.err.println("No settings file specified!");
		    System.exit(1);
		}
		if(i + 1 < args.length){
		    i++;
		    baseNetOut = args[i];
		}
		else{
		    System.err.println("Either no settings file or no output specified!");
		    System.exit(1);
		}
		propKill = 0.0;
		pkSet = true;
		rsSet = true;
		restartFromSnapshot = true;
		break;
	    case "-ma": // ma for maturation age
		maSet = true;
		if(i + 1 < args.length){
		    i++;
		    matAge = Integer.parseInt(args[i]);
		}
		break;
	    case "-en": // en for environment
		enSet = true;
		if(i + 1 < args.length){
		    i++;
		    envFile = Paths.get(args[i]);
		}
		break;
	    case "-fn": // for folder number (if queueing a lot of simulations with the same parameters, they might otherwise try to write to the same folders)
		fnSet = true;
		if(i + 1 < args.length){
		    i++;
		    fn = Integer.parseInt(args[i]);
		}
		break;
	    default:
		System.err.println("Unknown argument(s):\t" + args[i] + "\t Aborting.");
		System.exit(1);
	    }
	}
    }
}
