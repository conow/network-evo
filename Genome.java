import java.util.*;
import java.io.*;
import java.math.*;
import java.nio.file.*;

final class Genome{
    private Random r;
    final Chromosome[] gen;
    double ptRate;
    double rcRate;
    int numC;


    // this is stuff for makeReactions
    private Map<Integer, Reaction> mrResult = new HashMap<>();
    private Set<Integer> tempR = new HashSet<>();
    private Set<Integer> prodsM = new HashSet<>();
    private Set<Integer> remList = new HashSet<>();
    
    Genome(Genome g, Random r, double ptRate, double rcRate){
	this.r = r;
	gen = new Chromosome[g.gen.length];
	for(int i = 0; i < gen.length; i++){
	    gen[i] = new Chromosome(g.gen[i]);
	}
	this.numC = g.numC;
	this.ptRate = ptRate;
	this.rcRate = rcRate;
    }


    Genome(Genome g){
	r = new Random();
	ptRate = rcRate = -1;
	gen = new Chromosome[g.gen.length];
	this.numC = g.numC;
	for(int i = 0; i < gen.length; i++){
	    gen[i] = new Chromosome(g.gen[i]);
	}
    }


    Genome(Path fname, Random rand, double ptRate, double sceRate) throws IOException{
	this.ptRate = ptRate;
	this.rcRate = sceRate;
	r = rand;
	int tempC = 0;
	BufferedReader in = new BufferedReader(new FileReader(fname.toString()));
	ArrayList<Chromosome> tempGen = new ArrayList<Chromosome>();
	String s = "";
	while((s = in.readLine()) != null){
	    tempGen.add(new Chromosome(s, rcRate));
	    tempC += (tempGen.get(tempGen.size() - 1).dead())? 0 : 1;
	}
	numC = tempC;
	gen = new Chromosome[tempGen.size()];
	tempGen.toArray(gen);
    }

    Genome(String genS, Random rand, double ptRate, double sceRate){
	this.ptRate = ptRate;
	this.rcRate = sceRate;
	r = rand;
	String[] genSS = genS.trim().split("\n");
	gen = new Chromosome[genSS.length];
	int tempC = 0;
	for(int i = 0; i < genSS.length; i++){
	    gen[i] = new Chromosome(genSS[i], rcRate);
	    tempC += (gen[i].dead())? 0 : 1;
	}
	numC = tempC;
    }

    // This is for network analysis purposes only
    Genome(String genS){
	ptRate = -1;
	rcRate = -1;
	String[] genSS = genS.trim().split("\n");
	gen = new Chromosome[genSS.length];
	r = new Random();
	int tempC = 0;
	for(int i = 0; i < genSS.length; i++){
	    gen[i] = new Chromosome(genSS[i], rcRate);
	    tempC += (gen[i].dead())? 0 : 1;
	}
	numC = tempC;
    }
   
    public void initialize(Genome g1, Genome g2, Random rand, double ptRate, double rcRate){
	r = rand;
	int tempC = 0;
	for(int i = 0; i < gen.length; i+=2){
	    if(SimStart.verbose)
		System.err.println("g1");
	    gen[i].recInit(g1, i, r, rcRate, ptRate);
	    if(SimStart.verbose)
		System.err.println("g2");
	    gen[i+1].recInit(g2, i, r, rcRate, ptRate);
	    tempC += (gen[i].dead())? 0 : 1;
	    tempC += (gen[i+1].dead())? 0 : 1;
	}
	numC = tempC;
	this.ptRate = ptRate;
	this.rcRate = rcRate;
    }
    
    public Chromosome[] getChroms(){
	Chromosome[] result = new Chromosome[gen.length];
	for(int i = 0; i < gen.length; i++)
	    result[i] = new Chromosome(gen[i]);
	return result;
    }

    public boolean dead(){
	boolean result = false;
	for(Chromosome c : gen){
	    result = result || c.dead();

	}	    
	return result;
    }

    public Map<Integer, Reaction> makeReactions(LiveCell host){
	mrResult.clear();
	tempR.clear();
	boolean propd = false;
	int lc = 0;
	long start = System.nanoTime();
	double div = 1000000000.;
	prodsM.clear();
	for(Chromosome c : gen){
	    for(int i : c.geneLocView){
		long tt = System.nanoTime();
		int temp = -1;
		temp = Reaction.spec(c.getBGene(i));
		if(temp != -1){
		    if(mrResult.containsKey(temp)){
			mrResult.get(temp).update(c.getBGene(i), lc, i);
		    }
		    else{
			tt = System.nanoTime();
			Reaction tr = new Reaction(c.getBGene(i), lc, i);
			if(tr.exprSum != 0){
			    mrResult.put(temp, tr);
			    if(tr.type != 'i')
				tempR.add(temp);
			    else{
				if(host.pe.maxReplenish[tr.outs[0]] > 0){
				    prodsM.add(tr.outs[0]);
				    tr.sat = true;
				    propd = true;
				}
			    }
			}
		    }
		}
	    }
	    lc++;
	}

	remList.clear();
	while(propd){
	    propd = false;
	    for(int i : tempR){
		Reaction r = mrResult.get(i);
		switch(r.type){
		case 'r':
		    if(prodsM.contains(r.ins[0]) && prodsM.contains(r.ins[1])){
			propd = true;
			r.sat = true;
			prodsM.add(r.outs[0]);
			prodsM.add(r.outs[1]);
			remList.add(i);
		    }
		    break;
		case 'e':
		    if(prodsM.contains(r.ins[0])){
			propd = true;
			r.sat = true;
			remList.add(i);
		    }
		    break;
		default:
		    System.out.println("Is there an invalid gene somehow? " + r.type);
		    System.exit(1);
		}
	    }
	    for(int i : remList){
		tempR.remove(i);
	    }
	    remList.clear();
	}
	for(int i : mrResult.keySet()){
	    Reaction r = mrResult.get(i);
	    if(!r.sat)
		remList.add(i);
	}
	for(int i : remList)
	    mrResult.remove(i);
	return mrResult;
    }

    
    public String toString(){
	String result = "";
	for(int i = 0; i < gen.length; i++){
	    result += gen[i].toString() + "\n";
	}
	return result;
    }

    public void toFile(String s) throws IOException{
	FileWriter out = new FileWriter(s);
	for(Chromosome c : gen){
	    out.write(c.toString()+"\n");
	}
	out.close();
    }


    // given mean u and one time interval,
    // samples a point from the corresponding
    // poisson distribution. (Knuth's implementation)
    static int poisSamp(double u, Random rand){
	double eu = Math.pow(Math.E, -u);
	double p = 1;
	int result = 0;
	do{
	    result++;
	    p *= rand.nextDouble();
	}while(p > eu);
	
	return result-1;
    }
}	

